import sbt._

object Version {
  lazy val alpakkaCsv = "3.0.4"
  lazy val alpakkaXml = "3.0.4"
  lazy val alpakkaFile = "3.0.4"
  lazy val config = "1.3.3"
  lazy val commons = "2.6"
  lazy val commonsCodec = "1.10"
  lazy val htmlCleaner = "2.22"
  lazy val jenaLibs = "3.8.0"
  lazy val logback = "1.2.3"
  lazy val scalaLangMod = "2.0.0-M2"
  lazy val scalaLogging = "3.9.4"
  lazy val scalaTest = "3.2.9"
  lazy val synaptixVersion = "3.1.2-SNAPSHOT"
  lazy val scalaVersion = "2.13.8"
  lazy val analytix = "3.0-SNAPSHOT"
  lazy val franceTerme = "1.0.1-SNAPSHOT"
}

object Library {
  lazy val alpakkaXml = "com.lightbend.akka" %% "akka-stream-alpakka-xml" % Version.alpakkaXml
  lazy val alpakkaCsv = "com.lightbend.akka" %% "akka-stream-alpakka-csv" % Version.alpakkaCsv
  lazy val alpakkaFile = "com.lightbend.akka" %% "akka-stream-alpakka-file" % Version.alpakkaFile

  lazy val config: ModuleID = "com.typesafe" % "config" % Version.config
  lazy val commons = "commons-io" % "commons-io" % Version.commons
  lazy val commonsCodec = "commons-codec" % "commons-codec" % Version.commonsCodec
  lazy val htmlCleaner = "net.sourceforge.htmlcleaner" % "htmlcleaner" % Version.htmlCleaner
  lazy val jenaLibs: ModuleID = "org.apache.jena" % "apache-jena-libs" % Version.jenaLibs pomOnly()
  lazy val logbackClassic: ModuleID = "ch.qos.logback" % "logback-classic" % Version.logback

  lazy val scalaLangMod = "org.scala-lang.modules" %% "scala-xml" % Version.scalaLangMod
  lazy val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % Version.scalaLogging
  lazy val scalaTest: ModuleID = "org.scalatest" %% "scalatest" % Version.scalaTest
  lazy val synaptixAmqpToolKit = "com.mnemotix" %% "synaptix-amqp-toolkit" % Version.synaptixVersion
  lazy val synaptixRdfToolKit = "com.mnemotix" %% "synaptix-rdf-toolkit" % Version.synaptixVersion
  lazy val synaptixCore = "com.mnemotix" %% "synaptix-core" % Version.synaptixVersion
  lazy val synaptixHtmlToolkit = "com.mnemotix" %% "synaptix-http-toolkit" % Version.synaptixVersion
  lazy val synaptixCacheToolkit = "com.mnemotix" %% "synaptix-cache-toolkit" % Version.synaptixVersion
  lazy val synaptixJobToolkit = "com.mnemotix" % "synaptix-jobs-toolkit_2.13" % Version.synaptixVersion
  lazy val synaptixIndexToolkit ="com.mnemotix" % "synaptix-indexing-toolkit_2.13" % Version.synaptixVersion
  lazy val analytixCore = "com.mnemotix" % "analytix-commons_2.13" % Version.analytix
  lazy val ztZip = "org.zeroturnaround" % "zt-zip" % "1.14"

  lazy val franceTerme = "org.ddf" % "franceterm_2.13" % Version.franceTerme
}

object Dependencies {

  import Library._

  val alpakka = List(alpakkaXml, alpakkaCsv, alpakkaFile)
  val conf = List(config)
  val common = List(commons, commonsCodec)
  //val liftyLib = List(lifty)
  val log = List(logbackClassic, scalaLogging)
  val scala = List(scalaLangMod)
  val synaptix = List(synaptixCore, synaptixRdfToolKit, synaptixAmqpToolKit, synaptixHtmlToolkit, synaptixCacheToolkit,synaptixJobToolkit, synaptixIndexToolkit, analytixCore,
    franceTerme)
  val test = List(scalaTest)
  val misc = List(ztZip, htmlCleaner)

  // scalaLogging en compile
  // logback  en test

  val compileScope = conf ++ log ++ scala ++ alpakka ++ common ++ misc

  val testScope = (test).map(_ % "test")

  val providedScope = (
      synaptix
    )

  // Projects
  val core = compileScope ++ testScope ++ providedScope
}