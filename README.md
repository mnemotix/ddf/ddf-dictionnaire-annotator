# Fichier csv du dictionnaire

L'application analyse un fichier ``csv`` contenant les entrées du dictionnaire. 

## Organisation des colonnes

Les colonnes de ce fichier ``csv`` sont les prédicats du modèle DDF p. ex. ontolex:writtenRep, ontolex:otherForm, ontolex:phoneticRep, les prédicats des vocabulaires controllés p. ex. ddf:hasRegister, ddf:hasConnotation, ddf:hasFrequency. Pour indiquer les relations entre les mots nous utilisons l'URI de la relation p. ex. `http://data.dictionnairedesfrancophones.org/authority/sense-relation/synonym`, `http://data.dictionnairedesfrancophones.org/authority/sense-relation/hyponym`.
Dans la colonne nous mettons la ressource ou le littéral p. ex dans la colonne `ontolex:phoneticRep`, `abakɔs`, dans la colonne `ddf:hasConnotation`, `http://data.dictionnairedesfrancophones.org/authority/connotation/pejorativeConnotation`.

## Organisation des lignes

Chaque ligne du fichier ``csv`` correspond à une `Lexical:Entry`. Certains cas vont demander de dupliquer une ligne : 

* Pour un mot qui a plusieurs catégorie grammaticale (`ddf:hasPartOfSpeech`), p. ex

|  ontolex:writtenRep |  ddf:hasPartOfSpeech | skos:definition  | 
|---|---|---|
|  affairé | http://www.lexinfo.net/ontology/2.0/lexinfo#noun  |  Affairiste | 
| affairé  | http://www.lexinfo.net/ontology/2.0/lexinfo#adjective  | Affairiste  | 

* Pour une définition (`skos:definition`) qui a plusieurs exemple (`lexicog:UsageExample`), p. ex 

|  ontolex:writtenRep | ddf:hasPartOfSpeech | skos:definition | lexicog:UsageExample  | 
|---|---|---|---|
|  traiter | http://www.lexinfo.net/ontology/2.0/lexinfo#verb | Vendre ou acheter (une marchandise) hors du circuit commercial normal et à moindre prix.  |  Cette chemise coûte trop cher pour moi. Je tâcherai de la traiter avec le vendeur | 
| traiter  | http://www.lexinfo.net/ontology/2.0/lexinfo#verb | Vendre ou acheter (une marchandise) hors du circuit commercial normal et à moindre prix.  | Il y a des radios à traiter  |

* Pour une entrée qui a plusieurs étymologie (`ddf:hasItemAboutEtymology`).

En règle générale, nous préconisons la duplication d'une ligne pour un contenu textuel, un littéral.

En revanche, nous proposons, de séparer par un ``;`` les éléments qui proviennent d'un vocabulaire controllé, les mots liés p. ex

|  ontolex:writtenRep | ddf:formHasLocalisation | 
|---|---|
|  traiter | https://www.geonames.org/2395170;https://www.geonames.org/239880;https://www.geonames.org/2287781;https://www.geonames.org/2245662;https://www.geonames.org/2363686;https://www.geonames.org/203312 |

|  ontolex:writtenRep | http://data.dictionnairedesfrancophones.org/authority/sense-relation/synonym | 
|---|---|
| abacos | contre-veste;boubou abacos |

## Exemple de fichier

Un fichier exemple est disponible à l'adresse suivante : https://docs.google.com/spreadsheets/d/13_RqhlZdIzabrlWihNyPfEQyOVUlYoHePlGqM8as4lc/edit#gid=0
Les mots sont tirés de `Inventaire des particularités lexicales du français en Afrique noire: 1. A - B. - 1980. - 130 S.`
