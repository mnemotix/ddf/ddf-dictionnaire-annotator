package org.ddf.annotator

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

abstract class DDFAnnotatorException(message:String, cause:Option[Throwable]) extends Exception(message, cause.getOrElse(null))

class DDFCacheException(message:String, cause: Option[Throwable]) extends DDFAnnotatorException(message, cause)

class DDFSetterException(message:String, cause: Option[Throwable]) extends DDFAnnotatorException(message, cause)

class DDFExtractorException(message:String, cause:Option[Throwable]) extends DDFAnnotatorException(message, cause)

class DDFRecognizerException(message:String, cause:Option[Throwable]) extends DDFAnnotatorException(message, cause)

class DDFOutputException(message:String, cause:Option[Throwable]) extends DDFAnnotatorException(message, cause)

class DDFShaclException(message:String, cause:Option[Throwable]) extends DDFAnnotatorException(message, cause)

class DDFMappingException(message:String, cause:Option[Throwable]) extends DDFAnnotatorException(message, cause)

/*
abstract class WiktionnaireAnnotatorException(message:String, cause:Option[Throwable]) extends Exception(message, cause.getOrElse(null))

class WiktionnaireModelSetterException(message: String, cause:Option[Throwable]) extends WiktionnaireAnnotatorException(message, cause)

class WiktionnaireVocabularyLoaderException(message:String, cause:Option[Throwable]) extends WiktionnaireAnnotatorException(message, cause)

class WiktionnaireFileOutputException(message:String, cause: Option[Throwable]) extends WiktionnaireAnnotatorException(message, cause)

 */