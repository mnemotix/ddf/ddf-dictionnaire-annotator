/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.annotator.jobs

import akka.Done
import com.mnemotix.synaptix.core.NotificationValues
import com.mnemotix.synaptix.core.utils.{RandomNameGenerator, StringUtils}
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.index.elasticsearch.util.EsListener
import com.mnemotix.synaptix.jobs.api.SynaptixJob
import com.typesafe.scalalogging.LazyLogging
import org.ddf.annotator.{DDFMappingException, DDFSetterException}
import org.ddf.annotator.helpers.DDFDictionnaireConfig
import org.ddf.annotator.helpers.RDFSerializationUtil.createIRI
import org.ddf.annotator.services.{DDFDictionnaireAnnotator, DDFDictionnaireSenseRelationAnnotator, DDFSparqlExport, UploadFiles}
import org.ddf.franceTerm.helper.FranceTermConfig
import org.ddf.franceTerm.services.FranceTermConverter

import java.io.File
import scala.concurrent.duration.{Duration, DurationInt}
import scala.concurrent.{Await, Future}
import scala.reflect.io.Directory
import scala.util.{Failure, Success}

class DDFMapperJob extends SynaptixJob with LazyLogging {
  override val prefix: String = "ddf"
  override val label: String = "mapper"

  override lazy val id = RandomNameGenerator.haiku
  override val serviceName: String = "DDFMapperJob"

  override def process(): Future[Done] = {

    val filePath = {
      if (args.get("filePath").isDefined) {
        args.get("filePath").get.value.as[String]
      }
      else {
        DDFDictionnaireConfig.ddfLocation
      }
    }

    val dicoName = {
      if (args.get("dicoName").isDefined) {
        args.get("dicoName").get.value.as[String]
      }
      else {
        DDFDictionnaireConfig.dico
      }
    }

    val graphUri = {
      if (args.get("dicoName").isDefined) {
        args.get("dicoName").get.value.as[String]
        s"http://data.dictionnairedesfrancophones.org/dict/$dicoName/graph"
      }
      else {
        DDFDictionnaireConfig.graphUri
      }
    }

    val uriDictionnaire = {
      if (args.get("dicoName").isDefined) {
        val dicoName = args.get("dicoName").get.value.as[String]
        s"http://data.dictionnairedesfrancophones.org/resource/${dicoName}"
      }
      else DDFDictionnaireConfig.uriDictionnaire
    }

    val dictionnaireUriPrefix = {
      if (args.get("dicoName").isDefined) {
        val dicoName = args.get("dicoName").get.value.as[String]
        s"http://data.dictionnairedesfrancophones.org/dict/${dicoName}"
      }
      else DDFDictionnaireConfig.dictionnaireUriPrefix
    }

    val category = {
      if (args.get("category").isDefined) {
        Some(args.get("category").get.value.as[String])
      }
      else None
    }

    val esListener = new EsListener(id, dicoName, s"${ESConfiguration.prefix.getOrElse("")}synaptixjob", "DDF", category)
    Await.result(esListener.init(), 1.minute)
    registerListener(s"es-$prefix-$label", esListener)
    notify(NotificationValues.STARTUP)

    val backup = new DDFSparqlExport(Some(dicoName))

    update(s"sauvegarde du graphe http://data.dictionnairedesfrancophones.org/dict/$dicoName/graph")

    backup.init()
    logger.info(s"Exportation des trigs pour $dicoName")
    update(s"Exportation des trigs pour $dicoName")
    backup.exportTrig
    logger.info(s"Fin des exportations des trigs pour $dicoName")
    backup.shutdown()
    update(s"Fin des exportations des trigs pour $dicoName")

    val newfilepath = if (dicoName == "franceterme" && filePath.contains("xml")) {
      update(s"convertion du fichier xml de franceterme en CSV...")
      val xmlPath = new File(filePath).getAbsolutePath
      logger.info(s"le chemin du fichier $xmlPath")
      val converter = new FranceTermConverter(Some(xmlPath))
      val convertion = converter.convert
      convertion.onComplete {
        case Success(value) => {
          update(s"la conversion du fichier xml de franceterme en CSV est réussie")
        }
        case Failure(exception) => {
          throw new DDFMappingException(s"une erreur s'est passée lors de la conversion du fichier xml de franceterme en CSV", Some(exception))
          close()
        }
      }
      val r = Await.result(convertion, 5.minutes)
      s"${FranceTermConfig.dir}output/franceTerme.csv"
    }
    else filePath

    update("mapping...")

    try {
      val path = new File(newfilepath)
      val annotator = new DDFDictionnaireAnnotator(path.getAbsolutePath, dicoName= Some(dicoName), dictionnaireUriPrefix = Some(dictionnaireUriPrefix), uriDictionnaire= Some(uriDictionnaire), graphUri=Some(graphUri))
      val fut: (Future[Done], Future[Done]) = annotator.annotator
      val result: Future[Seq[Done]] = Future.sequence(Seq(fut._1, fut._2))
      annotator.init

      result.onComplete {
        case Success(value) => {
          logger.info(s"first passage is finished with value = $value")
          update("le processus de transformation en rdf des entrées simples est terminé ")
          annotator.shutdown
        }
        case Failure(e) => {
          if (e.isInstanceOf[DDFSetterException]) {
            annotator.shutdown
            logger.error(e.getMessage, e)
            notifyError(e.getMessage, Some(e))
            throw new DDFMappingException(e.getMessage, Some(e))
          }
          else {
            annotator.shutdown
            logger.error(s"a error occured during the first passage", e)
            notifyError(s"une erreur s'est produite lors du premier passage", Some(e))
            throw new DDFMappingException(s"une erreur s'est passée lors du premier passage", Some(e))
          }

        }
      }
      val finalResult = Await.result(result, Duration.Inf)

      val annotator2 = new DDFDictionnaireSenseRelationAnnotator(path.getAbsolutePath, dicoName= Some(dicoName), dictionnaireUriPrefix = Some(dictionnaireUriPrefix), uriDictionnaire= Some(uriDictionnaire),graphUri=Some(graphUri))
      val future2 = annotator2.annotate
      future2.onComplete {
        case Success(value) => {
          logger.info(s"second passage is finished with value = $value")
          update("le processus de transformation en rdf des entrées liées est terminé")
          annotator2.shutdown
        }
        case Failure(e) => {
          annotator2.shutdown
          logger.error(s"a error occured during the second passage", e)
          notifyError(s"une erreur s'est passée lors du second passage sur les relations", Some(e))
          throw new DDFMappingException(s"une erreur s'est passée lors du premier passage", Some(e))
        }
      }
      val finalResult2 = Await.result(future2, Duration.Inf)

      /**
       * drop graph
       */



      val rdfFileOutput = s"${DDFDictionnaireConfig.ddfDir}/$dicoName/rdf"
      val uploader = new UploadFiles(rdfFileOutput, Seq(createIRI(graphUri)), Some(esListener))
      uploader.init()
      val uploading = uploader.upload()

      uploading.onComplete {
        case Success(value) => {
          val path = StringUtils.removeTrailingSlashes(rdfFileOutput)
          new Directory(new File(s"${path}/zip/")).deleteRecursively()
          logger.info(s"uploading finished with value = $value")
          update("le processus de chargement des fichiers rdf est terminé")
          notify(NotificationValues.DONE)
          close()
        }
        case Failure(e) => {
          logger.error(s"une erreur s'est produite lors du chargement des fichiers", e)
          notifyError(s"une erreur s'est produite lors du chargement des fichiers", Some(e))
          throw new DDFMappingException(s"une erreur s'est produite lors du chargement des fichiers", Some(e))
        }
      }
      uploading
    }
    catch {
      case exception: Exception => {
        notifyError(s"${exception.getMessage}", Some(exception))
        close()
        throw new DDFMappingException(s"une erreur s'est passée lors du premier passage", Some(exception))
      }
    }
  }

  override def beforeProcess(): Unit = {
    init()
  }

  override def init(): Unit = {
    new File(DDFDictionnaireConfig.ddfDir).mkdir()
  }

  override def shutdown(): Unit = {}
}