package org.ddf.annotator.helpers

import com.typesafe.scalalogging.LazyLogging
import org.ddf.annotator.model.{SenseRelation, UsageExample}


/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Helper for [[org.ddf.annotator.services.DDFDictionnaireModelSetter]] instances
 */

object ModelSetterHelper extends LazyLogging {

  /**
   * uri lexical entry setter
   *
   * @param form the form entry
   * @param pos the part-of-speech
   * @param multiWordType the multiword type
   * @param termElement the term element type
   * @return uri of the lexical entry
   */

  def uriLexicalEntry(form: String, pos: Option[String], multiWordType: Option[String], termElement:Option[String],
                      dictionnaireUriPrefix: Option[String]): String = {
    if (pos.isDefined) {
      DDFDictionnaireUriFactory.ontolexWord(form, pos.get, dictionnaireUriPrefix)
    }
    else if (multiWordType.isDefined) {
      DDFDictionnaireUriFactory.ontolexMultiWordExpression(form, multiWordType.get, dictionnaireUriPrefix)
    }
    else if (termElement.isDefined){
      DDFDictionnaireUriFactory.ontolexTermElementExpression(form, termElement.get, dictionnaireUriPrefix)
    }
    else {
      DDFDictionnaireUriFactory.ontolexTermElementExpression(form, "http://data.dictionnairedesfrancophones.org/authority/multiword-type/unspecifiedPhrase", dictionnaireUriPrefix)
    }
  }

  /**
   * the entry type
   *
   * @param pos the part-of-speech
   * @param termElement the term element type
   * @param multiwordType the multiword type
   * @return uri of the type
   */

  def lexicalEntryType(pos:Option[String], termElement: Option[String], multiwordType: Option[String]): Option[String] = {
    val entryType = (pos.toList ++ termElement.toList ++ multiwordType.toList).filterNot(_.isEmpty)
    if (entryType.isEmpty) None else Some(entryType.head)
  }

  /**
   * usage exemple setter
   *
   * @param usageExample the string containing the usage example
   * @return
   */

  def usageExample(usageExample: Option[String]): Option[Seq[UsageExample]] = {
    if (!usageExample.isDefined) None
    else Some(
      exempleCitation(usageExample.get)
    )
  }

  /**
   * citation example setter
   *
   * @param usageCitation the string containing the usage citation
   * @return
   */

  def exempleCitation(usageCitation: String): Seq[UsageExample] = {
    usageCitation.split("\\|").map { uCitation =>
      UsageExample(uCitation, None)
    }.toSeq
  }

  /**
   * phonetic setter
   *
   * @param phoneticRep
   * @return
   */

  def phonetic(phoneticRep: String): Seq[String] = {
    phoneticRep.split("\\;").toSeq
  }

  /**
   *
   * @param otherForms
   * @return
   */

  def otherForms(otherForms: Option[String]): Option[Seq[String]] = {
    if (!otherForms.isDefined) None
    else Some(otherForms.get.split("\\;").toSeq)
  }

  /**
   *
   * @param notationSkos
   * @param words
   * @return
   */

  def senseRelation(notationSkos: String, words: String): SenseRelation = {
    val relatedWord: Array[String] = words.replaceAll("\\[\\[","").replaceAll("\\]\\]","").split("\\;")
    SenseRelation(notationSkos, relatedWord)
  }
}
