package org.ddf.annotator.helpers

import com.typesafe.config.{Config, ConfigFactory}

import scala.Predef.->
import scala.jdk.CollectionConverters.CollectionHasAsScala

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * DDF Dictionnaire Annotator config object
 * @author Pierre-René Lhérisson
 */


object DDFDictionnaireConfig {

  /**
   * annotator config
   */

  lazy val conf: Config = Option(ConfigFactory.load().getConfig("ddf")).getOrElse(ConfigFactory.empty())
  lazy val ddfDir: String = conf.getString("dir")
  lazy val ddfLocation: String = conf.getString("dictionnaire.location")
  lazy val rdfDumpDir: String = conf.getString("rdf.dump.dir")
  lazy val triplesPerDumpFile = conf.getInt("triples.per.file")
  lazy val uriDictionnaire = conf.getString("dictionnaire.url")
  lazy val dictionnaireUriPrefix = conf.getString("dictionaire.prefix")
  lazy val graphUri = conf.getString("dictionnaire.data.graph.uri")
  lazy val dico = conf.getString("dictionnaire.name")
  lazy val delimiter= conf.getString("delimiter")

  lazy val ontologyNamespace = conf.getConfig("rdf.namespaces").entrySet().asScala.map(keys => (keys.getKey, keys.getValue.unwrapped().toString)).toMap


  /**
   * rdf database config
   */

  lazy val rdf: Config = Option(ConfigFactory.load().getConfig("rdf")).getOrElse(ConfigFactory.empty())
  lazy val user = rdf.getString("user")
  lazy val password = rdf.getString("password")
  lazy val root = rdf.getString("root.uri")
  lazy val repositoryName = rdf.getString("repository.name")
}