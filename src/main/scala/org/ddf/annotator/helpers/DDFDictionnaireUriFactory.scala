package org.ddf.annotator.helpers

import java.math.BigInteger
import java.security.MessageDigest

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * DDF Dictionnaire Annotator uri factory
 * @author Pierre-René Lhérisson
 */

object DDFDictionnaireUriFactory {

  /** Creates the checksum of a given list of strings.
   *
   *  @param components list of string
   */

  def md5sum(components:String*): String = {
    val sb: StringBuilder = new StringBuilder
    for (c <- components) {
      if (c != null && c.length > 0) sb.append(c.trim)
    }
    val number: BigInteger = new BigInteger(1, MessageDigest.getInstance("MD5").digest(sb.toString.getBytes("UTF-8")))
    number.toString(16)
  }

  /** Creates the uri of a lexicog entry.
   *
   *  @param entry label of the entry
   */

  def lexicogEntry(entry: String, dictionnaireUriPrefix: Option[String]): String = {
    s"${dictionnaireUriPrefix.getOrElse(DDFDictionnaireConfig.dictionnaireUriPrefix)}/entry/${md5sum(createDictionnaireURL(entry, dictionnaireUriPrefix))}"
  }

  /** Creates the uri of an ontolexWord.
   *
   *  @param formLabel label of the dictionnary entry
   *  @param posType the part of speech of the entry
   */

  def ontolexWord(formLabel: String, posType: String, dictionnaireUriPrefix: Option[String]): String = {
    s"${dictionnaireUriPrefix.getOrElse(DDFDictionnaireConfig.dictionnaireUriPrefix)}/word/${md5sum(formLabel, posType)}"
  }

  /** Creates the uri of an Inflectable.
   *
   *  @param formLabel label of the dictionnary entry
   *  @param termElement the term element of the entry
   */

  def ontolexAffix(formLabel: String, termElement: String, dictionnaireUriPrefix: Option[String]): String = {
    s"${dictionnaireUriPrefix.getOrElse(DDFDictionnaireConfig.dictionnaireUriPrefix)}/affix/${md5sum(formLabel, termElement)}"
  }

  /** Creates the uri of an multi word expression.
   *
   *  @param formLabel label of the dictionnary entry
   *  @param multiType the part of speech of the entry
   */

  def ontolexMultiWordExpression(formLabel: String, multiType: String, dictionnaireUriPrefix: Option[String]): String = {
    s"${dictionnaireUriPrefix.getOrElse(DDFDictionnaireConfig.dictionnaireUriPrefix)}/multi-word/${md5sum(formLabel, multiType)}"
  }

  /** Create the uri of an termElement
   *
   */

  def ontolexTermElementExpression(formLabel: String, termType: String, dictionnaireUriPrefix: Option[String]): String = {
    s"${dictionnaireUriPrefix.getOrElse(DDFDictionnaireConfig.dictionnaireUriPrefix)}/term-element/${md5sum(formLabel, termType)}"
  }

  /** Creates the uri of an lexical Sens.
   *
   *  @param skosDefinitionContent the content of the definition with the usage and the bibliographical citation
   */

  def ontolexLexicalSense(skosDefinitionContent: String, uriEntry: String, dictionnaireUriPrefix: Option[String], pos: Option[String], multipos:Option[String], transitivity: Option[String]): String = {
    s"${dictionnaireUriPrefix.getOrElse(DDFDictionnaireConfig.dictionnaireUriPrefix)}/sense/${md5sum(skosDefinitionContent, uriEntry, pos.getOrElse(""), multipos.getOrElse(""), transitivity.getOrElse(""))}"
  }

  /** Creates the uri of an form.
   *
   *  @param ontolexFormLabel the form label
   */

  def ontolexForm(ontolexFormLabel: String, dictionnaireUriPrefix: Option[String]): String = {
    s"${dictionnaireUriPrefix.getOrElse(DDFDictionnaireConfig.dictionnaireUriPrefix)}/form/${md5sum(ontolexFormLabel)}"
  }

  /** Creates the uri of an post content.
   *
   *  @param siocPostContent the string content of a post
   */

  def siocPost(siocPostContent: String, dictionnaireUriPrefix: Option[String]): String = {
    s"${dictionnaireUriPrefix.getOrElse(DDFDictionnaireConfig.dictionnaireUriPrefix)}/sioc-post/${md5sum(siocPostContent)}"
  }

  /** Creates the uri of an online contribution.
   *
   *  @param uriParentNode the uri of the parent node (can be the uri of a form, a lexical sens, lexical entry)
   *  @param postContent the string content of a post
   */

  def ddfOnlineContribution(uriParentNode: String, postContent: String, dictionnaireUriPrefix: Option[String]): String = {
    s"${dictionnaireUriPrefix.getOrElse(DDFDictionnaireConfig.dictionnaireUriPrefix)}/online-contribution/${md5sum(uriParentNode, postContent)}"
  }

  /** Creates the uri of an online contribution.
   *
   *  @param lexicogEntryURI the uri of the lexicog entry
   *  @param lexicalSenseURI the uri of the lexical sens entry
   *  @param relationTypeURI the uri of relation type
   */

  // LexicogEntry URI + LexicalSense URI + Relation type URI
  def ddfSemanticRelation(lexicogEntryURI: String, lexicalSenseURI: String, relationTypeURI: String, dictionnaireUriPrefix: Option[String]) = {
    s"${dictionnaireUriPrefix.getOrElse(DDFDictionnaireConfig.dictionnaireUriPrefix)}/relation/${md5sum(lexicogEntryURI,lexicalSenseURI, relationTypeURI)}"
  }

  /** Creates the uri of an usage example.
   *
   *  @param lexicogUsageExampleValue the content of the example
   */

  def lexicogUsageExample(lexicogUsageExampleValue: String, dictionnaireUriPrefix: Option[String]): String = {
    s"${dictionnaireUriPrefix.getOrElse(DDFDictionnaireConfig.dictionnaireUriPrefix)}/example/${md5sum(lexicogUsageExampleValue)}"
  }

  /** Creates the uri of an inflection.
   *
   *  @param ddfInflectablePOSURI the uri of the inflectable pos
   *  @param gender the uri of the gender
   *  @param number the uri of the number
   */

  def ddfInflection(ddfInflectablePOSURI: String, gender: String, number: String, dictionnaireUriPrefix: Option[String]): String = {
    s"${dictionnaireUriPrefix.getOrElse(DDFDictionnaireConfig.dictionnaireUriPrefix)}/inflection/${md5sum(ddfInflectablePOSURI, gender, number)}"
  }

  /** Creates the uri of a verbal inflection.
   *
   *  @param verbURI the uri of the verb
   *  @param moodType the uri of the mood
   *  @param tenseType the uri of the tense type
   *  @param personType the uri of the person
   */

  def ddfVerbalInflection(verbURI: String, moodType: String, tenseType: String, personType: String, dictionnaireUriPrefix: Option[String]) = {
    s"${dictionnaireUriPrefix.getOrElse(DDFDictionnaireConfig.dictionnaireUriPrefix)}/verbal-inflection/${md5sum(verbURI, moodType, tenseType, personType)}"
  }

  /** Creates the uri of the dictionnary base on the uri of the dictionnary.
   *
   *  @param entry the form entry
   */

  def createDictionnaireURL(entry: String, dictionnaireUriPrefix: Option[String]) = {
    s"${dictionnaireUriPrefix.getOrElse(DDFDictionnaireConfig.dictionnaireUriPrefix)}$entry"
  }
}