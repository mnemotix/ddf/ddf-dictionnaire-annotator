/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.annotator.helpers

object TextHelper {

  def htmlIt(text: String): String = {
    val afterBold = toBold(text)
    val afterItalic = toItalic(afterBold)
    val afterHref = toHref(afterItalic)
    externalLink(afterHref)
  }

  def toBold(txt: String): String = {
    if (txt.contains("'''''")) {
      val firstReplacement = txt.replaceFirst("'''''", "''<b>")
      val secondReplacement = firstReplacement.replaceFirst("'''", "</b>")
      toBold(secondReplacement)
    }
    else if (txt.contains("'''")) {
      val firstReplacement = txt.replaceFirst("'''", "<b>")
      val secondReplacement = firstReplacement.replaceFirst("'''", "</b>")
      toBold(secondReplacement)
    }
    else txt
  }

  def toItalic(txt: String): String = {
    if (txt.contains("''")) {
      val firstReplacement = txt.replaceFirst("''", "<i>")
      val secondReplacement = firstReplacement.replaceFirst("''", "</i>")
      toItalic(secondReplacement)
    }
    else txt
  }

  def externalLink(txt: String): String = {

    val check = "\\[\\[(.*?)\\]\\]".r.findFirstIn(txt)


    if (check.nonEmpty) {
      val toURI = check.get
      if (toURI.contains(" ")) {
        val s = toURI.split("\\s", 2)
        val replacement =s"""<a href="${s(0).replaceAll("\\[","")}">${
          s(1).replaceAll("\\]", "")}</a>"""
        val result = txt.replace(toURI, replacement)
        externalLink(result)
      }
      else {
        val replacement = s"""<a href="${toURI.
          replaceAll("\\[","").
          replaceAll("\\]","")}">${toURI.
          replaceAll("\\]","").
          replaceAll("\\[","")}</a>"""
        val result = txt.replace(toURI, replacement)
        toHref(result)
      }
    }
    else txt
  }

  def toHref(txt: String): String = {
    val check = """\[\[(.*?)\]\](\w+(?=(\s|\p{Punct})))?""".r.findFirstIn(txt)
    if (check.nonEmpty) {
      val toURI = check.get
      if (toURI.matches("\\[\\[(.*?)\\|(.*?)\\]\\](.*)?")) {
        val replacement = toHrefTwoPipeTreatment(toURI)
        val result = txt.replace(toURI, replacement)
        toHref(result)
      }
      else {
        val replacement = toHrefOnePipeTreatment(toURI)
        val result = txt.replace(toURI, replacement)
        toHref(result)
      }
    }
    else txt
  }

  val listofParsed = Seq("[[s:", "[[w:]]", "[[n:", "[[ws:]]", "[[b:]]")

  def toHrefTwoPipeTreatment(toUri: String) = {
    if (toUri.matches("\\[\\[(.*?)\\:(.*?)\\]\\]")) {
      val s = toUri.split("\\|")
      val replacement = s(1).replaceAll("]]","")
      replacement
    }
    else {
      val s = toUri.split("\\|")
      // /form/cylindrique
      val replacement = s"""<a href="/form/${s(0).
        replaceAll("\\[\\[","")}\">${s(1).
        replaceAll("\\]\\]","")}</a>"""
      replacement
    }
  }

  def toHrefOnePipeTreatment(toUri: String) = {
    if (toUri.matches("\\[\\[(.*?)\\:(.*?)\\]\\]")) {
      val replacement = toUri.replaceAll("\\[\\[(.*?)\\:","").replaceAll("\\]\\]","")
      replacement
    }
    else {
      val replacement = s"""<a href="/form/${toUri.
        replaceAll("\\[\\[","").
        replaceAll("\\]\\](\\w+)?","")}">${toUri.
        replaceAll("\\]\\]","").
        replaceAll("\\[\\[","")}</a>"""
      replacement
    }
  }
}