/**
 * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.annotator.helpers

import org.eclipse.rdf4j.model.{IRI, Literal, Model, ValueFactory}
import org.eclipse.rdf4j.model.impl.SimpleValueFactory
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.rio.{RDFFormat, Rio}

import java.io.{ByteArrayOutputStream, FileOutputStream}

object RDFSerializationUtil {
  val vf:ValueFactory = SimpleValueFactory.getInstance

  lazy val STRINGIRIDATATYPE = createIRI("http://www.w3.org/2001/XMLSchema#string")
  lazy val INTIRIDATATYPE = createIRI("http://www.w3.org/2001/XMLSchema#int")
  lazy val DATEIRIDATATYPE = createIRI("http://www.w3.org/2001/XMLSchema#date")
  lazy val URLDATATYPE = createIRI("http://www.w3.org/2001/XMLSchema#danyURI")
  lazy val BOOLEANDATATYPE = createIRI("http://www.w3.org/2001/XMLSchema#boolean")
  lazy val FLOATDATATYPE = createIRI("http://www.w3.org/2001/XMLSchema#float")

  def merge(m1: Model, m2: Model): Model = {
    m1.getNamespaces.addAll(m2.getNamespaces)
    m1.addAll(m2)
    m1
  }

  def merge(m: Seq[Model]): Model = {
    m.foldLeft(emptyModel)(merge(_,_))
  }

  def emptyModel: Model = new ModelBuilder().build()

  def createIRI(resource: String): IRI = vf.createIRI(resource)

  def createLiteral(value: Float): Literal = {
    vf.createLiteral(value)
  }

  def createLiteral(value: Boolean): Literal = {
    vf.createLiteral(value)
  }

  def nameSpace(builder: ModelBuilder): ModelBuilder = {
    def modelBuilderHelper(couples: Seq[(String, String)], builder: ModelBuilder):ModelBuilder = {
      couples match {
        case Nil => builder
        case x :: tail =>
          if (x._1 != "default") modelBuilderHelper(tail, builder.setNamespace(x._1, x._2))
          else modelBuilderHelper(tail, builder.setNamespace("", x._2))
      }
    }
    val list = DDFDictionnaireConfig.ontologyNamespace.toList
    modelBuilderHelper(list, builder)
  }

  def toFile(model: Model, fileName: String): Unit = {
    val file = new FileOutputStream(s"${DDFDictionnaireConfig.rdfDumpDir}${fileName}.trig")
    Rio.write(model, file, RDFFormat.TRIG)
  }

  def toTriG(model: Model): String = {
    val os: ByteArrayOutputStream = new ByteArrayOutputStream()
    Rio.write(model, os, org.eclipse.rdf4j.rio.RDFFormat.TRIG)
    val bytes = os.toString
    os.close()
    bytes
  }

  def toJsonLd(model: Model): String = {
    val os: ByteArrayOutputStream = new ByteArrayOutputStream()
    Rio.write(model, os, org.eclipse.rdf4j.rio.RDFFormat.JSONLD)
    val bytes = os.toString
    os.close()
    bytes
  }
}
