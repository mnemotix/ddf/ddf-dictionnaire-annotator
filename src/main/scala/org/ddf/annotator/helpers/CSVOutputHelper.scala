/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.annotator.helpers

import org.ddf.annotator.model.Entry
import org.ddf.annotator.services.recognizer.LexicalSensAnnotationsRecogs.vocsLexical
import org.htmlcleaner.HtmlCleaner

object CSVOutputHelper {

  def toCsv(entries: Seq[Entry]): List[Array[String]] = {
    entries.map { entry =>
      Array(entry.wasDerivedFrom.getOrElse(""), entry.form,
        if (entry.lentries.otherForms.isDefined) entry.lentries.otherForms.get.mkString(";") else "",
        if (entry.lentries.canonicalForm.isDefined) entry.lentries.canonicalForm.get.phoneticRep.mkString(";") else "",
        entry.lentries.pos.getOrElse(""),
        entry.lentries.multiWordType.getOrElse(""),
        entry.lentries.gender.getOrElse(""),
        entry.lentries.number.getOrElse(""),
        entry.lentries.transitivity.getOrElse(""),
        entry.lentries.lexicalEntryType.getOrElse(""),
        entry.lentries.etymology.getOrElse(""),
        if (entry.lentries.sense.isDefined) "" else "",
        if (entry.lentries.sense.isDefined) "" else "",
        if (entry.lentries.sense.isDefined) entry.lentries.sense.get.definition else "",
        if (entry.lentries.sense.isDefined) cleanHtml(entry.lentries.sense.get.definition) else "",
        if (entry.lentries.sense.isDefined) {
          if (entry.lentries.sense.get.usageExamples.isDefined) {
            entry.lentries.sense.get.usageExamples.get.map(_.bibliographicalCitation.getOrElse("")).mkString(";")
          }else ""
        }  else "",
        if (entry.lentries.sense.isDefined) {
          if (entry.lentries.sense.get.usageExamples.isDefined) {
            entry.lentries.sense.get.usageExamples.get.map(cit => cleanHtml(cit.bibliographicalCitation.getOrElse(""))).mkString(";")
          } else ""
        } else "",
        if (entry.lentries.sense.isDefined) entry.lentries.sense.get.hasLocalisation.getOrElse("") else "",
        if (entry.lentries.sense.isDefined) "" else "",
        // ,"","","","","","",
        if (entry.lentries.sense.isDefined)  vocsLexical(entry.lentries.sense.get.vocsLexical,"domain") else "",
        if (entry.lentries.sense.isDefined)  vocsLexical(entry.lentries.sense.get.vocsLexical,"register") else "",
        if (entry.lentries.sense.isDefined)  vocsLexical(entry.lentries.sense.get.vocsLexical,"connotation") else "",
        if (entry.lentries.sense.isDefined)  vocsLexical(entry.lentries.sense.get.vocsLexical,"frequency") else "",
        if (entry.lentries.sense.isDefined)  vocsLexical(entry.lentries.sense.get.vocsLexical,"sociolect") else "",
        if (entry.lentries.sense.isDefined)  vocsLexical(entry.lentries.sense.get.vocsLexical,"temporality") else "",
        if (entry.lentries.sense.isDefined) entry.lentries.sense.get.scientificName.getOrElse("") else "",
        if (entry.lentries.sense.isDefined) entry.lentries.sense.get.aboutSens.getOrElse("") else "",
        if (entry.lentries.sense.isDefined)  vocsLexical(entry.lentries.sense.get.vocsLexical,"associativeRel") else "",
        if (entry.lentries.sense.isDefined)  vocsLexical(entry.lentries.sense.get.vocsLexical,"synonym") else ""
      )
    }.toList
  }

  def cleanHtml(text: String): String = {
    val cleaner = new HtmlCleaner()
    cleaner.clean(text).getText.toString
  }
}
