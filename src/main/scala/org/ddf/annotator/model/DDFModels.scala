package org.ddf.annotator.model

import java.net.URI

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * DDF Dictionnaire model ontology in the form of case classes
 * @author Pierre-René Lhérisson
 */

case class VocabularyEntity(voc: String, notation: String, concept: String, property: String, conceptLabel: String)

case class VocsLexical(vocab: String, content: String)

case class UsageExample(example: String,bibliographicalCitation: Option[String])

case class Sense(definition: String, usageExamples: Option[Seq[UsageExample]], vocsLexical: Seq[VocsLexical], aboutSens: Option[String], scientificName: Option[String], hasLocalisation: Option[String])

case class CanonicalForm(label: String, phoneticRep: Seq[String])

case class SenseRelation(notationSkos: String, relationWord: Seq[String])

case class LexicalEntry(uri: URI, lexicalEntryType: Option[String], sense: Option[Sense], canonicalForm: Option[CanonicalForm], etymology: Option[String],
                        otherForms: Option[Seq[String]], senseRelation: Seq[Option[SenseRelation]],
                        pos: Option[String], multiWordType: Option[String], gender: Option[String], number: Option[String], transitivity: Option[String], formHasLocalisation: Option[String])

case class Entry(uri: URI, form: String, aboutForm: Option[String], text: String, lentries: LexicalEntry, wasDerivedFrom: Option[String], hasLexicographicResource: Option[String]=None)

object Entry {
  implicit class toRDF(entry: Entry) {

  }
}