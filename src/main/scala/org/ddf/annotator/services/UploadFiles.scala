/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.annotator.services

import akka.{Done, NotUsed}
import akka.actor.ActorSystem
import akka.stream.alpakka.file.ArchiveMetadata
import akka.stream.alpakka.file.scaladsl.Archive
import akka.stream.{IOResult, RestartSettings}
import akka.stream.scaladsl.{FileIO, Flow, RestartFlow, Sink, Source}
import com.mnemotix.synaptix.core.utils.{RandomNameGenerator, StringUtils}
import com.mnemotix.synaptix.core.{MonitoredService, NotificationValues}
import com.mnemotix.synaptix.rdf.client.{RDFClient, RDFFormats, RDFLoadException}
import com.mnemotix.synaptix.rdf.client.models.RDFClientWriteConnection
import org.ddf.annotator.helpers.DDFDictionnaireConfig
import org.eclipse.rdf4j.model.IRI
import com.mnemotix.analytix.commons.utils.FileUtils
import com.mnemotix.synaptix.index.elasticsearch.util.EsListener

import java.io.File
import java.nio.file.Paths
import java.util.concurrent.atomic.AtomicInteger
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

class UploadFiles(rdfFilesPath: String, contextIRI: Seq[IRI], esListener: Option[EsListener]=None)(implicit system: ActorSystem) extends MonitoredService {

  override val serviceName: String = "UploadFiles"
  implicit val ec: ExecutionContextExecutor = system.dispatcher

  implicit val writeConnection: RDFClientWriteConnection = RDFClient.getWriteConnection(DDFDictionnaireConfig.repositoryName)

  override def init(): Unit = {RDFClient.init()}
  override def shutdown(): Unit = RDFClient.shutdown()

  if(esListener.isDefined) {
    registerListener(s"es-uploading", esListener.get)

  }

  def upload(): Future[Done] = {
    notify(NotificationValues.UPDATE, Some(("suppression des graphes...")))
    val deleteFut =  RDFClient.update(s"DROP GRAPH <${contextIRI.head.toString}>")
    Await.result(deleteFut, 40.minutes)
    deleteFut.onComplete {
      case Success(_) => {
        logger.info(s"Le graphe ${contextIRI.head.toString} a été supprimé avec succès")
        notify(NotificationValues.UPDATE, Some(s"le graphe ${contextIRI.head.toString} a été supprimé"))
      }
      case Failure(exception) => {
        notifyError(s"une erreur est sourvenue  lors de la suppression du graphe ${contextIRI.head.toString}", Some(exception))
        throw RDFLoadException(s"An error occured during the ${contextIRI.head.toString} drop request", exception)
      }
    }
    notify(NotificationValues.UPDATE, Some("archivage des fichiers rdf..."))
    val path = StringUtils.removeTrailingSlashes(rdfFilesPath)
    val zipped = zip()
    val fut = Future.sequence(zipped)
    Await.result(fut, 10.minutes)
    notify(NotificationValues.UPDATE, Some(s"les fichiers sont archivés dans le répertoire ${path}/zip/..."))
    val files = FileUtils.getListOfFiles(new File(s"${path}/zip/"), List("zip"))
    notify(NotificationValues.UPDATE, Some(s"chargement de ${files.size} fichier(s)..."))
    upload(files, contextIRI.map(_.toString):_*)
  }

  private def upload(files: Seq[File], context: String*): Future[Done] = {
    val counter = new AtomicInteger(0)
    val source: Source[File, NotUsed] = Source.fromIterator(() => files.iterator)
    val flow = Flow[File].map { file =>
      // MuscatConfig.rdfMuscatGraphNameRaw
      val futLoaded = RDFClient.load(file, None, RDFFormats.TRIG, context:_*)

      futLoaded.onComplete {
        case Success(_) => {
          val uploaded = counter.getAndAdd(1)
          notifyProgress(uploaded, Some(files.size), Some(s"nous avons chargé ${file.getName} avec succès"))
          logger.info(s"We upload ${file.getName} successfully")

        }
        case Failure(exception) => {
          logger.info(s"An error occured during ${file.getName} uploading")
          notifyError("une erreur est sourvenue lors du chargement de fichiers", Some(exception))
          throw RDFLoadException(s"An error occured during ${file.getName} uploading", exception)
        }
      }
      Await.result(futLoaded, 30.minutes)
    }
    val repeatableFlow = RestartFlow.onFailuresWithBackoff(settings) { () => flow }
    source.via(repeatableFlow).runWith(Sink.ignore)
  }

  private def zip() = {
    val path = StringUtils.removeTrailingSlashes(rdfFilesPath)
    val directory = new File(s"${path}/zip")
    if (!directory.exists()) directory.mkdir()

    val files = FileUtils.getListOfFiles(new File(s"${path}"), List("trig"))
    val toZip = files.map(_.getName)
    toZip.grouped(5).map { list =>
      val files = list.map(file => new File(s"${path}/$file"))
      val name = RandomNameGenerator.haiku
      archive(files, s"${path}/zip/$name.zip")
    }
  }

  private def archive(files: Seq[File], zipFile: String): Future[IOResult] = {
    val streams = files.map { file =>
      (ArchiveMetadata(file.getName), FileIO.fromPath(file.toPath))
    }
    Source(streams.toList).via(Archive.zip())
      .runWith(FileIO.toPath(Paths.get(zipFile)))
  }

  val settings = RestartSettings(
    minBackoff = 3.seconds,
    maxBackoff = 30.seconds,
    randomFactor = 0.2
  ).withMaxRestarts(20, 5.minutes)
}
