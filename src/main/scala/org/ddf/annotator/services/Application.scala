/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.annotator.services

import akka.Done
import org.ddf.annotator.helpers.DDFDictionnaireConfig

import java.io.File
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success}

object Application extends App {

  implicit val executionContext =  ExecutionContext.global

  def map(): Unit = {
    val path = new File(DDFDictionnaireConfig.ddfLocation)
    val annotator = new DDFDictionnaireAnnotator(path.getAbsolutePath)
    val fut: (Future[Done], Future[Done]) = annotator.annotator
    val result: Future[Seq[Done]] = Future.sequence(Seq(fut._1, fut._2))
    annotator.init

    result.onComplete {
      case Success(value) => {
        println(s"first passage is finished with value = $value")
        annotator.shutdown
      }
      case Failure(e) => {
        println(s"error : $e")
        e.printStackTrace
      }
    }
    val finalResult = Await.result(result, Duration.Inf)

    val annotator2 = new DDFDictionnaireSenseRelationAnnotator(path.getAbsolutePath)
    val future2 = annotator2.annotate
    future2.onComplete {
      case Success(value) => {
        println(s"second passage is finished with value = $value")
        annotator2.shutdown
      }
      case Failure(e) => e.printStackTrace
    }
    val finalResult2 = Await.result(future2, Duration.Inf)
  }
  map()
}
