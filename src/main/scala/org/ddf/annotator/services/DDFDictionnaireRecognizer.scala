package org.ddf.annotator.services

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Flow
import com.mnemotix.synaptix.cache.RocksDBStore
import com.typesafe.scalalogging.LazyLogging
import org.ddf.annotator.DDFRecognizerException
import org.ddf.annotator.helpers.DDFDictionnaireConfig
import org.ddf.annotator.helpers.RDFSerializationUtil.nameSpace
import org.ddf.annotator.model.Entry
import org.ddf.annotator.services.generator.{DDFRdfAnnotationsGenerator, SenseRelationsRdfAnnotationsGenerator}
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.util.ModelBuilder

import scala.collection.mutable
import scala.concurrent.{ExecutionContextExecutor, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * the service that convert the ddf model to Lifty Annotations
 *
 * @param posType
 * @param system
 * @param mat
 * @param ec
 */

class DDFDictionnaireRecognizer(posType:mutable.Map[String, String], dictionnaireUriPrefix: Option[String], uriDictionnaire:Option[String], graphUri: Option[String])(implicit system: ActorSystem, ec: ExecutionContextExecutor) extends LazyLogging {
  def recognize: Flow[Entry, Model, NotUsed] = {
    try {
      Flow[Entry].filter(_.lentries.sense.isDefined).filter(_.lentries.sense.get.definition.trim != "").mapAsync(4) { entry =>
        val builder = new ModelBuilder()
        val model = nameSpace(builder).namedGraph(graphUri.getOrElse(DDFDictionnaireConfig.graphUri))
        DDFRdfAnnotationsGenerator(entry, posType, model, dictionnaireUriPrefix, uriDictionnaire)
        val m: Model = model.build()
        Future(m)
      }
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the recognition process", t)
        throw new DDFRecognizerException("An error occured during the recognition process", Some(t))
    }
  }

  def recognizeSenseRelation(rocksDBStore: RocksDBStore, dictionnaireUriPrefix: Option[String]): Flow[Entry, Model, NotUsed] = {
    try {
      Flow[Entry].mapAsync(4) { entry =>
        val builder = new ModelBuilder()
        val model = nameSpace(builder).namedGraph(graphUri.getOrElse(DDFDictionnaireConfig.graphUri))
        SenseRelationsRdfAnnotationsGenerator.generator(entry, rocksDBStore, model, dictionnaireUriPrefix)
        val m = model.build()
        Future(m)
      }
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the recognition process", t)
        throw new DDFRecognizerException("An error occured during the recognition process", Some(t))
    }
  }
}