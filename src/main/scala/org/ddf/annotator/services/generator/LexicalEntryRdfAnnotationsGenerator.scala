package org.ddf.annotator.services.generator

import java.net.URI
import org.ddf.annotator.model.{CanonicalForm}
import org.ddf.annotator.services.recognizer.LexicalEntryAnnotationsRecogs
import org.eclipse.rdf4j.model.util.ModelBuilder

import scala.collection.mutable

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Lexical entry annotations generator
 */

object LexicalEntryRdfAnnotationsGenerator {

  /**
   * to check if a cell contains a value or not
   *
   * @param x content of a csv cell
   * @return boolean
   */

  def isEmpty(x: String): Boolean = x == null || x.trim.isEmpty || x.contains("#N/A") || x.size == 0

  /**
   * Generator of Lifty Annotations for Part of speech
   *
   * @param lexicalEntryURI
   * @param pos
   * @param posType
   * @return
   */

  def posGenerator(lexicalEntryURI: URI, pos: Option[String], posType: mutable.Map[String, String], model: ModelBuilder) = {
    if (pos.isDefined) {
      if (!isEmpty(pos.get)) {
        LexicalEntryAnnotationsRecogs.pos(lexicalEntryURI, pos, posType, model)
      }
    }
  }

  /**
   * Generator of Lifty Annnotations for multiword type
   *
   * @param lexicalEntryURI
   * @param multiWordType
   * @return
   */

  def multiwordGenerator(lexicalEntryURI: URI, multiWordType: Option[String], model: ModelBuilder) = {
    if (multiWordType.isDefined) {
      if (!isEmpty(multiWordType.get)) {
        LexicalEntryAnnotationsRecogs.multiWord(lexicalEntryURI, multiWordType, model)
      }
    }
  }

  /**
   * Generator of Lifty Annotation for gender
   *
   * @param lexicalEntryURI
   * @param gender
   * @return
   */

  def genderGenerator(lexicalEntryURI: URI, gender: Option[String], model: ModelBuilder) = {
    if (gender.isDefined) {
      if (!isEmpty(gender.get)) {
        LexicalEntryAnnotationsRecogs.gender(lexicalEntryURI, gender, model)
      }
    }
  }

  /**
   * Generator of Lifty Annotation for number
   *
   * @param lexicalEntryURI
   * @param number
   * @return
   */

  def numberGenerator(lexicalEntryURI: URI, number: Option[String], model: ModelBuilder) = {
    if (number.isDefined) {
      if (!isEmpty(number.get)) {
        LexicalEntryAnnotationsRecogs.number(lexicalEntryURI, number, model)
      }
    }
  }

  /**
   * Generator of Lifty Annotation for transitivity
   *
   * @param lexicalEntryURI
   * @param transitivity
   * @return
   */

  def transitivityGenerator(lexicalEntryURI: URI, transitivity: Option[String], model: ModelBuilder) = {
    if (transitivity.isDefined) {
      if (!isEmpty(transitivity.get)) {
        LexicalEntryAnnotationsRecogs.transitivity(lexicalEntryURI, transitivity, model)
      }
    }
  }

  /**
   * Generator of Lifty Annotations for other forms
   *
   * @param lexicalEntryURI
   * @param otherForms
   * @return
   */

  def otherFormsGenerator(lexicalEntryURI: URI, otherForms: Option[Seq[String]], model: ModelBuilder, dictionnaireUriPrefix: Option[String]) = {
    if (otherForms.toSeq.flatten.size > 0) {
      LexicalEntryAnnotationsRecogs.otherFormAnnotations(otherForms.get, lexicalEntryURI, model, dictionnaireUriPrefix)
    }
  }

  /**
   * Generator of Lifty Annotations for pronounciation
   *
   * @param lexicalEntryURI
   * @param canonicalForm
   * @return
   */

  def pronounciationsGenerator(FormURI: URI, canonicalForm: Option[CanonicalForm], model: ModelBuilder) = {
    if (canonicalForm.isDefined) {
      LexicalEntryAnnotationsRecogs.pronounciationAnnotations(FormURI, canonicalForm.get.phoneticRep, model)
    }
  }

  /**
   * Generator of Lifty Annotations for Localisation of forms
   *
   * @param lexicalEntryURI
   * @param formHasLocalisation
   * @return
   */

  def formsLocationsGenerator(lexicalEntryURI: URI, formHasLocalisation: Option[String], model: ModelBuilder) = {
    if (formHasLocalisation.isDefined) {
      if (!isEmpty(formHasLocalisation.get)) {
        LexicalEntryAnnotationsRecogs.formHasLocalisation(lexicalEntryURI, formHasLocalisation, model)
      }
    }
  }

  /**
   * Generator of Lifty Annotations for etymology
   *
   * @param lexicalEntryURI
   * @param etymology
   * @return
   */

  def etymologyGenerator(lexicalEntryURI: URI, etymology: Option[String], model: ModelBuilder, dictionnaireUriPrefix: Option[String]) = {
    if (etymology.isDefined) {
      if (!isEmpty(etymology.get)) {
        LexicalEntryAnnotationsRecogs.etymologyAnnotations(lexicalEntryURI, etymology.get, model, dictionnaireUriPrefix)
      }
    }
  }
}