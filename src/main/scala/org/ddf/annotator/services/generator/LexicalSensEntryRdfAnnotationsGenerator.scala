package org.ddf.annotator.services.generator

import java.net.URI
import org.ddf.annotator.model.{Entry, LexicalEntry, UsageExample}
import org.ddf.annotator.services.recognizer.LexicalSensAnnotationsRecogs
import org.eclipse.rdf4j.model.util.ModelBuilder

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Lexical sens annotations generator
 */

object LexicalSensEntryRdfAnnotationsGenerator {

  /**
   * lexical sens Lifty Annotations generator
   *
   * @param entry
   * @param lentry
   * @return
   */

  def lexicalSensGenerator(entry: Entry, lentry: LexicalEntry, model: ModelBuilder, dictionnaireUriPrefix: Option[String]) = {
    val sense = lentry.sense
    if (sense.isDefined) {
      val lexicalSensURI = LexicalSensAnnotationsRecogs.lexicalSensURIGenerate(sense.get.definition, sense.get.usageExamples, entry.uri, dictionnaireUriPrefix, entry.lentries.pos, entry.lentries.multiWordType, entry.lentries.transitivity)
      val lexicalSens = LexicalSensAnnotationsRecogs.lexicalSensGenerate(lentry.uri, lexicalSensURI, model)
      val skosDefinition = LexicalSensAnnotationsRecogs.skosDefinitionGenerate(sense.get.definition, entry.wasDerivedFrom, lexicalSensURI, model)
      val usageExampleCitation = usageExampleGenerator(sense.get.usageExamples, lexicalSensURI, model,dictionnaireUriPrefix)
      val vocsLexical = LexicalSensAnnotationsRecogs.vocsLexical(lexicalSensURI, lentry.sense.get.vocsLexical, model)
      val scientificName = LexicalSensAnnotationsRecogs.scientificName(sense.get.scientificName, lexicalSensURI, model)
      val aboutSens = LexicalSensAnnotationsRecogs.aboutSens(lexicalSensURI, lentry.sense.get.aboutSens, model, dictionnaireUriPrefix)
      val localisation = LexicalSensAnnotationsRecogs.lexicalSensLocation(lexicalSensURI, lentry.sense.get.hasLocalisation, model)
    }
  }

  /**
   * Usage example Lifty Annotations
   *
   * @param usageExamples
   * @param lexicalSensURI
   * @return
   */

  def usageExampleGenerator(usageExamples: Option[Seq[UsageExample]], lexicalSensURI: URI, model: ModelBuilder, dictionnaireUriPrefix: Option[String]) = {
    if (usageExamples.isDefined) {
      usageExamples.get.foreach { usageExemple =>
        val exempleURI: URI = LexicalSensAnnotationsRecogs.exampleURIGenerate(usageExemple.example, usageExemple.bibliographicalCitation, dictionnaireUriPrefix)
        val usageExample = LexicalSensAnnotationsRecogs.usageExampleGenerate(usageExemple.example, lexicalSensURI, exempleURI, model)
        val source = if (usageExemple.bibliographicalCitation.isDefined) Some(LexicalSensAnnotationsRecogs.bibliographicalCitationGenerate(exempleURI, usageExemple.bibliographicalCitation.get, model)) else None
      }
    }
  }
}