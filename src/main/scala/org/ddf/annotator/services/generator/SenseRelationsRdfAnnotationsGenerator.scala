package org.ddf.annotator.services.generator

import java.net.URI
import com.mnemotix.synaptix.cache.RocksDBStore
import org.ddf.annotator.model.Entry
import org.ddf.annotator.services.recognizer.SenseRelationsAnnotationsRecogs
import org.eclipse.rdf4j.model.util.ModelBuilder

import scala.concurrent.{ExecutionContext}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object SenseRelationsRdfAnnotationsGenerator {

  def generator(entry: Entry, rocksDBStore: RocksDBStore, model: ModelBuilder, dictionnaireUriPrefix: Option[String])(implicit ec: ExecutionContext) = {
    entry.lentries.senseRelation.filter(sr => sr.isDefined).foreach { sr =>
      sr.get.relationWord.filter(word => filterWords(rocksDBStore, word).isDefined).foreach { word =>
        val uri = filterWords(rocksDBStore, word)
        val uriRelationsWith = new String(uri.get)
        SenseRelationsAnnotationsRecogs.senseRelations(entry.uri, entry.lentries.uri, sr.get.notationSkos, new URI(uriRelationsWith), model, dictionnaireUriPrefix)
      }
    }
  }

  def filterWords(rocksDBStore: RocksDBStore, word: String)(implicit ec: ExecutionContext) = {
    val futget = rocksDBStore.get(word)
    futget
  }
}