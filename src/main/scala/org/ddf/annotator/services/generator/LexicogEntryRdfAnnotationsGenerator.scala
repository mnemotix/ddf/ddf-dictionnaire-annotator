package org.ddf.annotator.services.generator

import org.ddf.annotator.helpers.DDFDictionnaireConfig
import org.ddf.annotator.helpers.RDFSerializationUtil.createIRI
import org.ddf.annotator.model.Entry
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Lexicog Entry Annotations Generator
 */

object LexicogEntryRdfAnnotationsGenerator {

  /**
   * Lexicog entry annotations generator
   *
   * @param entry
   * @return Seq[LiftyAnnotation]
   */

  def lexicogEntryAnnotations(entry: Entry, model: ModelBuilder, uriDictionnaire: Option[String]) = {

    if (entry.wasDerivedFrom.isDefined) {
      model.subject(entry.uri.toString)
        .add("http://www.w3.org/ns/prov#wasDerivedFrom", createIRI(entry.wasDerivedFrom.get))
    }

    model.subject(entry.uri.toString)
      .add(RDF.TYPE, createIRI("http://www.w3.org/ns/lemon/lexicog#Entry"))
      .add("http://data.dictionnairedesfrancophones.org/ontology/ddf#hasLexicographicResource", createIRI(s"${uriDictionnaire.getOrElse(DDFDictionnaireConfig.uriDictionnaire)}"))
  }
}
