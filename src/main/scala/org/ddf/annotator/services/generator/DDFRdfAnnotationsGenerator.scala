package org.ddf.annotator.services.generator

import java.net.URI
import org.ddf.annotator.helpers.DDFDictionnaireUriFactory
import org.ddf.annotator.model.Entry
import org.ddf.annotator.services.recognizer.{LexicalEntryAnnotationsRecogs}
import org.eclipse.rdf4j.model.util.ModelBuilder

import scala.collection.mutable

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Generate annotations for a DDF dictionary
 */

object DDFRdfAnnotationsGenerator {
  def apply(entry: Entry, posType: mutable.Map[String, String], model: ModelBuilder, dictionnaireUriPrefix: Option[String], uriDictionnaire: Option[String]) = {
    grammar(entry, posType, model)
    lexicogEntry(entry, model, uriDictionnaire)
    forms(entry, model, dictionnaireUriPrefix)
    formsOptions(entry, model, dictionnaireUriPrefix)
    etymology(entry, model)
    location(entry, model, dictionnaireUriPrefix)

    LexicalSensEntryRdfAnnotationsGenerator.lexicalSensGenerator(entry, entry.lentries, model, dictionnaireUriPrefix)
  }

  /**
   * return annotations for lexicog entry
   *
   * @param entry
   * @return Seq[LiftyAnnotation]
   */

  def lexicogEntry(entry: Entry, model: ModelBuilder, uriDictionnaire: Option[String]) = {
    val lexicogEntry =  LexicogEntryRdfAnnotationsGenerator.lexicogEntryAnnotations(entry, model, uriDictionnaire)
    val describes = LexicalEntryAnnotationsRecogs.lexicalEntryAnnotations(entry.uri, entry.lentries.uri, model)
  }

  /**
   * return annotations for form
   *
   * @param entry
   * @return Seq[LiftyAnnotation]
   */

  def forms(entry: Entry, model: ModelBuilder, dictionnaireUriPrefix: Option[String]) = {
    val uri: URI = entry.lentries.uri
    LexicalEntryAnnotationsRecogs.formAnnotations(uri, entry.form, entry.aboutForm, model, dictionnaireUriPrefix)
    LexicalEntryRdfAnnotationsGenerator.otherFormsGenerator(uri, entry.lentries.otherForms, model, dictionnaireUriPrefix)
  }

  /**
   * returns annotations for forms pronounciations and forms locations
   *
   * @param entry
   * @return Seq[LiftyAnnotation]
   */

  def formsOptions(entry: Entry, model: ModelBuilder, dictionnaireUriPrefix: Option[String]) = {
    val uri = new URI(DDFDictionnaireUriFactory.ontolexForm(entry.form.trim, dictionnaireUriPrefix))
    LexicalEntryRdfAnnotationsGenerator.pronounciationsGenerator(uri,entry.lentries.canonicalForm, model)
    //LexicalEntryAnnotationsGenerator.formsLocationsGenerator(uri, entry.lentries.formHasLocalisation).toSeq.flatten
  }

  // todo remove the gdt hack

  /**
   * return annotations for POS, multiword type, gender, number, transitivity
   *
   * @param entry
   * @param posType
   * @return Seq[LiftyAnnotation]
   */

  def grammar(entry: Entry, posType: mutable.Map[String, String], model: ModelBuilder)  = {
    val uri = entry.lentries.uri
    val classForm = LexicalEntryAnnotationsRecogs.extractClassForm(entry.form)
    val posSeq = LexicalEntryRdfAnnotationsGenerator.posGenerator(uri, entry.lentries.pos, posType, model)

    /* GTT HACK TO DELETE */
    val gft = if (classForm == "http://www.w3.org/ns/lemon/ontolex#MultiWordExpression") LexicalEntryRdfAnnotationsGenerator.multiwordGenerator(uri, entry.lentries.multiWordType, model)
    else  posSeq

    LexicalEntryRdfAnnotationsGenerator.genderGenerator(uri, entry.lentries.gender, model)
    LexicalEntryRdfAnnotationsGenerator.numberGenerator(uri, entry.lentries.number, model)
    LexicalEntryRdfAnnotationsGenerator.transitivityGenerator(uri, entry.lentries.transitivity,model)

  }

  /**
   * return annotations for etymology
   *
   * @param entry
   * @return
   */

  def etymology(entry: Entry, model: ModelBuilder, dictionnaireUriPrefix: Option[String] = None) = {
    val uri = entry.lentries.uri
    LexicalEntryRdfAnnotationsGenerator.etymologyGenerator(uri, entry.lentries.etymology, model, dictionnaireUriPrefix)
  }

  /**
   * return annotations for locations
   *
   * @param entry
   * @return
   */

  def location(entry: Entry, model: ModelBuilder, dictionnaireUriPrefix: Option[String]) = {
    //val uri = entry.lentries.uri
    val formUri = new URI(DDFDictionnaireUriFactory.ontolexForm(entry.form, dictionnaireUriPrefix))
    LexicalEntryRdfAnnotationsGenerator.formsLocationsGenerator(formUri, entry.lentries.formHasLocalisation, model)
  }
}