/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.annotator.services

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, ClosedShape}
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, RunnableGraph, Sink}
import com.typesafe.scalalogging.LazyLogging
import org.ddf.annotator.helpers.DDFDictionnaireConfig

import scala.collection.mutable
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor}

@deprecated
class DictionnaireAnnotatorCounter(filePath: String, dictionnaireUriPrefix: Option[String] = None, uriDictionnaire:Option[String]= None) extends LazyLogging {

  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  implicit val system: ActorSystem = ActorSystem("AnnotationSystem")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val rdfReader = new DDFDictionnaireRdfReader()
  rdfReader.init
  val vocs: mutable.ListBuffer[(String, String)] = rdfReader.vocsBlocked
  val posType: mutable.Map[String, String] = rdfReader.posTypeBlocked
  val senseRelation: mutable.Seq[String] = rdfReader.senseRelationBlocked

  lazy val extractor = new DDFDictionnaireExtractor(filePath, Some(','.toByte))
  lazy val setter = new DDFDictionnaireModelSetter(posType, senseRelation, vocs, dictionnaireUriPrefix, DDFDictionnaireConfig.dico)
 // lazy val recognizer = new DDFDictionnaireRecognizer(posType, dictionnaireUriPrefix, uriDictionnaire)
// dicoName.getOrElse(s"${DDFDictionnaireConfig.dico}")
  def init= {logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")}
/*
  def countSimple = {
    val lexicogEntrySink = Sink.seq[Seq[String]]
    val lexicalEntrySink = Sink.seq[Seq[String]]
    val lexicalSensSink = Sink.seq[Seq[String]]
    val canonicalFormSink = Sink.seq[Seq[String]]

    val inflectablePOSSink = Sink.seq[Seq[String]]
    val verbSink = Sink.seq[Seq[String]]
    val invariablePOSSink = Sink.seq[Seq[String]]

    val EtymSink = Sink.seq[Seq[String]]

    RunnableGraph.fromGraph(GraphDSL.create(lexicogEntrySink, lexicalEntrySink, lexicalSensSink, canonicalFormSink, inflectablePOSSink, verbSink, invariablePOSSink)((_,_,_,_,_,_,_)) { implicit builder =>
      (lexicogES, lexicalES, lexicalSS, cononicalFS, inflectablePS, verbS, invariablePS) =>

        import GraphDSL.Implicits._


        val broadcast4 = builder.add(Broadcast[Seq[LiftyAnnotation]](7))
        val filterLexicogEntrySink: Flow[Seq[LiftyAnnotation], Seq[String], NotUsed] = Flow[Seq[LiftyAnnotation]].map(_.filter(liftyAnnotation => liftyAnnotation.obj.stringValue() == "http://www.w3.org/ns/lemon/lexicog#Entry").map(_.resource.toString))
        val filterLexicalEntrySink = Flow[Seq[LiftyAnnotation]].map(_.filter(liftyAnnotation => liftyAnnotation.property == "http://www.w3.org/ns/lemon/ontolex#canonicalForm").map(_.resource.toString))
        val filterLexicalSensSink = Flow[Seq[LiftyAnnotation]].map(_.filter(liftyAnnotation => liftyAnnotation.obj.stringValue() == "http://www.w3.org/ns/lemon/ontolex#LexicalSense").map(_.resource.toString))
        val filterCanonicalFormSink = Flow[Seq[LiftyAnnotation]].map(_.filter(liftyAnnotation => liftyAnnotation.obj.stringValue() == "http://www.w3.org/ns/lemon/ontolex#Form").map(_.resource.toString))
        val filterInflectablePOSSink =  Flow[Seq[LiftyAnnotation]].map(_.filter(liftyAnnotation => liftyAnnotation.obj.stringValue() == "http://data.dictionnairedesfrancophones.org/ontology/ddf#InflectablePOS").map(_.resource.toString))
        val filterVerbSink = Flow[Seq[LiftyAnnotation]].map(_.filter(liftyAnnotation => liftyAnnotation.obj.stringValue() == "http://data.dictionnairedesfrancophones.org/ontology/ddf#Verb").map(_.resource.toString))
        val filterInvariableSink = Flow[Seq[LiftyAnnotation]].map(_.filter(liftyAnnotation => liftyAnnotation.obj.stringValue() == "http://data.dictionnairedesfrancophones.org/ontology/ddf#InvariablePOS").map(_.resource.toString))


        //"http://data.dictionnairedesfrancophones.org/ontology/ddf#hasItemAboutEtymology"

        extractor.extract ~>  setter.setter ~> recognizer.recognize ~> broadcast4 ~> filterLexicogEntrySink ~> lexicogES
        broadcast4 ~>  filterLexicalEntrySink ~> lexicalES
        broadcast4 ~> filterLexicalSensSink ~> lexicalSS
        broadcast4 ~> filterCanonicalFormSink ~> cononicalFS
        broadcast4 ~>  filterInflectablePOSSink ~> inflectablePS
        broadcast4 ~> filterVerbSink ~> verbS
        broadcast4 ~> filterInvariableSink ~> invariablePS

        ClosedShape
    }).run()
  }

 */
}