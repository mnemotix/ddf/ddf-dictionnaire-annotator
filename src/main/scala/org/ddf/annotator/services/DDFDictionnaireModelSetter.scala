package org.ddf.annotator.services

import java.net.URI
import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.synaptix.core.MonitoredService
import com.mnemotix.synaptix.core.utils.StringUtils
import org.ddf.annotator.DDFSetterException
import org.ddf.annotator.helpers.{DDFDictionnaireUriFactory, ModelSetterHelper, SerializerHelper}
import org.ddf.annotator.model.{CanonicalForm, Entry, LexicalEntry, Sense, SenseRelation, UsageExample, VocsLexical}
import org.ddf.annotator.services.generator.LexicalEntryRdfAnnotationsGenerator

import java.util.concurrent.atomic.AtomicInteger
import scala.collection.mutable
import scala.concurrent.{ExecutionContextExecutor, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * the service that populate the ddf model
 *
 * @param posType
 * @param senseRelationObj
 * @param vocs
 * @param ec
 */

class DDFDictionnaireModelSetter(posType: mutable.Map[String, String], senseRelationObj: mutable.Seq[String],
                                 vocs: mutable.ListBuffer[(String, String)], dictionnaireUriPrefix: Option[String], dicoName: String)(implicit ec: ExecutionContextExecutor) extends MonitoredService {

  def mapValue(aMap: Map[String, String], key: String): Option[String] = {

    if  (aMap.get(key).isDefined) {
      if (aMap.get(key).get.trim.isEmpty) {
        None
      }
      else Some(aMap.get(key).get.trim)
    }
    else None

   // if (aMap.get(key).isDefined && !aMap.get(key).get.trim.isEmpty && aMap.get(key).get != " " && aMap.get(key).get != "") {
    //if (!aMap.get(key).get.trim.isEmpty) Some(aMap.get(key).get.trim) else None
  //} else None
  }


  def setter: Flow[Map[String, String], Entry, NotUsed] = {
    Flow[Map[String, String]].mapAsync(500) { aMap =>
      //logger.info(s"${aMap.get("ontolex:writtenRep").get.trim}")
      val writtenRep = aMap.get("ontolex:writtenRep").get.trim
      val definition = aMap.get("skos:definition").get.trim
      val lexicogEntry: String = DDFDictionnaireUriFactory.lexicogEntry(writtenRep, dictionnaireUriPrefix)
      val aboutForm = SerializerHelper.mapValue(aMap, "ddf:aboutForm")

      if (mapValue(aMap, "ddf:hasLexicographicResource").isDefined) {
        if (!mapValue(aMap, "ddf:hasLexicographicResource").get.contains(StringUtils.stripAccents(dicoName))) throw new DDFSetterException(s"La colonne ddf:hasLexicographicResource n'est pas valide pour la writtenRep $writtenRep", None)
      }

      Future(Entry(new URI(lexicogEntry), writtenRep, aboutForm, definition, lexicalEntry(aMap, posType), mapValue(aMap, "prov:wasDerivedFrom"), mapValue(aMap, "ddf:hasLexicographicResource")))
    }
  }

  def lexicalEntry(aMap: Map[String, String], posType: mutable.Map[String, String]): LexicalEntry = {
    val pos = mapValue(aMap, "ddf:hasPartOfSpeech")
    val mwt = mapValue(aMap, "ddf:multiWordType")
    val te = mapValue(aMap, "lexinfo:termElement")

   // logger.info(s"${aMap.get("ontolex:writtenRep").get} - ${pos} - ${mwt} - ${te}")

    val uri = ModelSetterHelper.uriLexicalEntry(aMap.get("ontolex:writtenRep").get, pos, mwt, te, dictionnaireUriPrefix)

    LexicalEntry(URI.create(uri),
      ModelSetterHelper.lexicalEntryType(mapValue(aMap, "ddf:hasPartOfSpeech"), mapValue(aMap, "lexinfo:termElement"), mapValue(aMap, "ddf:multiWordType")),
      sense(aMap),
      canonicalForm(aMap),
      aMap.get("ddf:hasItemAboutEtymology"),
      ModelSetterHelper.otherForms(aMap.get("ontolex:otherForm")),
      senseRelation(aMap),
      if (aMap.get("ddf:hasPartOfSpeech").isDefined) Some(aMap.get("ddf:hasPartOfSpeech").get) else None,
      if (aMap.get("ddf:multiWordType").isDefined) Some(aMap.get("ddf:multiWordType").get) else None,
      if (aMap.get("ddf:hasGender").isDefined) Some(aMap.get("ddf:hasGender").get) else None,
      if (aMap.get("ddf:hasNumber").isDefined) Some(aMap.get("ddf:hasNumber").get) else None,
      if (aMap.get("ddf:hasTransitivity").isDefined) Some(aMap.get("ddf:hasTransitivity").get) else None,
      if (aMap.get("ddf:formHasLocalisation").isDefined) Some(aMap.get("ddf:formHasLocalisation").get) else None
    )
  }

  def sense(aMap: Map[String, String]): Option[Sense] = {
    if (!aMap.get("skos:definition").isDefined) None
    else Some(Sense(aMap.get("skos:definition").get.trim, usageExample(aMap), vocsLexical(aMap), aMap.get("ddf:hasItemAboutSense"), mapValue(aMap, "ddf:hasScientificName"), mapValue(aMap, "ddf:hasLocalisation")))
  }

  def vocsLexical(aMap: Map[String, String]): Seq[VocsLexical] = {
    vocs.map { voc =>
      if (!aMap.get(voc._1).isDefined || LexicalEntryRdfAnnotationsGenerator.isEmpty(aMap.get(voc._1).get)) None
      else Some(VocsLexical(voc._2, aMap.get(voc._1).get.trim))
    }.flatten.toSeq
  }

  def usageExample(aMap: Map[String, String]): Option[Seq[UsageExample]] = {
    ModelSetterHelper.usageExample(SerializerHelper.mapValue(aMap, "lexicog:usageExample"))
  }

  def canonicalForm(aMap: Map[String, String]): Option[CanonicalForm] = {
    if (!aMap.get("ontolex:phoneticRep").isDefined) None
    else Some(CanonicalForm(aMap.get("ontolex:writtenRep").get, ModelSetterHelper.phonetic(aMap.get("ontolex:phoneticRep").get.trim)))
  }

  def senseRelation(aMap: Map[String, String]): Seq[Option[SenseRelation]] = {
    senseRelationObj.map { notationSkos =>
      val words = aMap.get(notationSkos)
      if (!words.isDefined) None
      else Some(ModelSetterHelper.senseRelation(notationSkos, words.get.trim))
    }.toSeq
  }

  def ddfType(aMap: Map[String, String]): Option[String] = {
    if (aMap.get("ddf:multiWordType").isDefined) Some(aMap.get("ddf:multiWordType").get.trim)
    else if (aMap.get("ddf:multiWordType").isDefined) Some(aMap.get("ddf:multiWordType").get.trim)
    else None
  }

  override val serviceName: String = "DDFDictionnaireModelSetter"

  override def init(): Unit = {}

  override def shutdown(): Unit = {}
}