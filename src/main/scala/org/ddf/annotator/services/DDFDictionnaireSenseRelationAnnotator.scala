package org.ddf.annotator.services

import akka.Done
import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, ClosedShape}
import akka.stream.scaladsl.{GraphDSL, RunnableGraph, Sink}
import org.ddf.annotator.helpers.DDFDictionnaireConfig

import scala.collection.mutable
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class DDFDictionnaireSenseRelationAnnotator(filePath: String, dicoName: Option[String] = None,dictionnaireUriPrefix: Option[String] = None, uriDictionnaire:Option[String]= None, graphUri:Option[String]= None) {

  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  implicit val system: ActorSystem = ActorSystem("AnnotationSenseRelationSystem")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val rdfReader = new DDFDictionnaireRdfReader()
  rdfReader.init
  val vocs: mutable.ListBuffer[(String, String)] = rdfReader.vocsBlocked
  val posType: mutable.Map[String, String] = rdfReader.posTypeBlocked
  val senseRelation: mutable.Seq[String] = rdfReader.senseRelationBlocked

  lazy val extractor = new DDFDictionnaireExtractor(filePath, Some(','))
  lazy val setter = new DDFDictionnaireModelSetter(posType, senseRelation, vocs, dictionnaireUriPrefix, dicoName.getOrElse(s"${DDFDictionnaireConfig.dico}"))
  lazy val recognizer = new DDFDictionnaireRecognizer(posType, dictionnaireUriPrefix, uriDictionnaire, graphUri)
  lazy val cacheManager = new DDFDictionnaireDBCacheManager()

  lazy val dumper = new DDFDictionnaireFileOutput(dicoName = dicoName, graphUri = graphUri, true)

  lazy val topHeadSink = Sink.ignore
  lazy val cache = DDFDictionnaireDBCacheManager.initDB(dicoName.getOrElse(DDFDictionnaireConfig.dico))


  def annotate: Future[Done] = {
    cache.init
    RunnableGraph.fromGraph(GraphDSL.create(topHeadSink){ implicit builder =>
      (sink) =>
        import GraphDSL.Implicits._
        extractor.extract ~> setter.setter ~> recognizer.recognizeSenseRelation(cache, dictionnaireUriPrefix).grouped(100000) ~> dumper.dump ~> sink.in
        ClosedShape
    }).run()
  }

  def shutdown: Unit = {
    cache.shutdown()
    system.terminate()
    materializer.shutdown()
  }
}
