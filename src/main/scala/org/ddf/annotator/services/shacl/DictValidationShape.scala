/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.annotator.services.shacl

object DictValidationShape {

  def shacl = """@prefix wik: <http://data.dictionnairedesfrancophones.org/resource/wiktionnaire> .
                |@prefix owl: <http://www.w3.org/2002/07/owl#> .
                |@prefix ddf: <http://data.dictionnairedesfrancophones.org/ontology/ddf#> .
                |@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
                |@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
                |@prefix lexinfo: <http://www.lexinfo.net/ontology/2.0/lexinfo#> .
                |@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
                |@prefix lemon: <http://lemon-model.net/lemon#> .
                |@prefix dct: <http://purl.org/dc/terms/> .
                |@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
                |@prefix xml: <http://www.w3.org/XML/1998/namespace> .
                |@prefix lexicog: <http://www.w3.org/ns/lemon/lexicog#> .
                |@prefix ontolex: <http://www.w3.org/ns/lemon/ontolex#> .
                |@prefix prov: <http://www.w3.org/ns/prov-o-20130430#> .
                |@prefix sioc: <http://rdfs.org/sioc/ns#> .
                |@prefix wikt: <http://data.dictionnairedesfrancophones.org/dict/wikt/> .
                |@prefix sh: <http://www.w3.org/ns/shacl#> .
                |@prefix : <http://data.dictionnairedesfrancophones.org/shacl/> .
                |
                |
                |
                |# Info minimale pour une entrée (lexicog:Entry):
                |# - A: 1 forme
                |# - B: 1 définition
                |# - C: 1 classe de mot
                |
                |# Chaque lexicog:Entry doit avoir au moins un lien lexicog:describes et un lien ddf:hasLexicographicResource
                |:EntryShape a sh:NodeShape ;
                |    sh:targetClass lexicog:Entry ;
                |    sh:property [
                |        sh:path lexicog:describes ;
                |        sh:minCount 1;
                |    ]
                |    .
                |
                |# Chaque sous-classe de ontolex:LexicalEntry doit avoir au moins 1 ontolex:canonicalForm et 1 definition
                |:LEntryShape a sh:NodeShape ;
                |    sh:targetClass ontolex:Word, ontolex:Affix, ontolex:MultiWordExpression, ddf:Verb, ddf:InflectablePOS, ddf:InvariablePOS ;
                |    sh:property [
                |        sh:path ontolex:canonicalForm ;
                |        sh:minCount 1;
                |    ] ;
                |    sh:property [
                |        sh:path ontolex:sense ;
                |        sh:minCount 1;
                |    ] .
                |
                |# Chaque ontolex:Form  doit avoir exactement un lien ontolex:writtenRep
                |:FormShape a sh:NodeShape ;
                |    sh:targetClass ontolex:Form ;
                |    sh:property [
                |        sh:path  ontolex:writtenRep ;
                |        sh:minCount 1;
                |        sh:maxCount 1;
                |    ].
                |
                |# Chaque ontolex:LexicalSense doit avoir au max un lien skos:definition, et une longueur mini de 1
                |:LSense a sh:NodeShape ;
                |    sh:targetClass ontolex:LexicalSense ;
                |    sh:property [
                |        sh:path  skos:definition ;
                |        sh:minCount 1;
                |        sh:minLength 1;
                |    ] .
                |""".stripMargin
}

/*
# Check Classe de mot
# =>  Chaque sous-classe de ontolex:LexicalEntry doit avoir au moins 1 lien vers un ensemble de classe de mot dédiés:
:LEntryWordClassShape a sh:NodeShape ;
    sh:targetClass ontolex:Word, ddf:Verb, ddf:InvariablePOS, ddf:InflectablePOS, ontolex:MultiWordExpression, ontolex:Affix;
    sh:property [
        sh:path [sh:alternativePath (ddf:hasPartOfSpeech ddf:multiWordType lexinfo:termElement)]	;
        sh:minCount 1;
    ] .
 */