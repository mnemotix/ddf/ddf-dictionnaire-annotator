package org.ddf.annotator.services.recognizer

import java.net.URI
import org.ddf.annotator.helpers.{DDFDictionnaireConfig, DDFDictionnaireUriFactory}
import org.ddf.annotator.helpers.RDFSerializationUtil.createIRI
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object SenseRelationsAnnotationsRecogs {

  def senseRelations(lexicogEntryUri: URI, lexicalEntryURI: URI, vocab: String, uriRelationsWith: URI, model: ModelBuilder, dictionnaireUriPrefix: Option[String]) = {

    val uriRelation = DDFDictionnaireUriFactory.ddfSemanticRelation(lexicogEntryUri.toString, lexicalEntryURI.toString, vocab, dictionnaireUriPrefix)

    model.subject(lexicalEntryURI.toString)
      .add("http://data.dictionnairedesfrancophones.org/ontology/ddf#lexicalEntryHasSemanticRelationWith", createIRI(uriRelation))

    model.subject(uriRelation)
      .add(RDF.TYPE, createIRI("http://data.dictionnairedesfrancophones.org/ontology/ddf#SemanticRelation"))
      .add("http://data.dictionnairedesfrancophones.org/ontology/ddf#hasSemanticRelationType", createIRI(vocab))
      .add("http://data.dictionnairedesfrancophones.org/ontology/ddf#semanticRelationOfEntry", createIRI(uriRelationsWith.toString))
  }
}