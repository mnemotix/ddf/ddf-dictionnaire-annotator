package org.ddf.annotator.services.recognizer

import java.net.URI
import org.ddf.annotator.helpers.RDFSerializationUtil.createIRI
import org.ddf.annotator.helpers.{DDFDictionnaireConfig, DDFDictionnaireUriFactory, TextHelper}
import org.ddf.annotator.model.{UsageExample, VocsLexical}
import org.ddf.annotator.services.generator.LexicalEntryRdfAnnotationsGenerator
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

import scala.collection.mutable

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Lexical Sens Annotations setter
 */

object LexicalSensAnnotationsRecogs {

  /**
   *
   * lexical sens uri
   *
   * @param definition
   * @param usageExamples
   * @return
   */

  def lexicalSensURIGenerate(definition: String, usageExamples: Option[Seq[UsageExample]], uriLexicalEntry: URI, dictionnaireUriPrefix: Option[String],  pos: Option[String], multipos:Option[String], transitivity: Option[String]): URI = {
    val usageExampleString = if (usageExamples.isDefined) usageExamples.get.mkString else ""
    URI.create(DDFDictionnaireUriFactory.ontolexLexicalSense(s"$definition ${usageExampleString}", uriLexicalEntry.toString, dictionnaireUriPrefix, pos, multipos, transitivity))
  }

  /**
   * example uri generator
   *
   * @param example
   * @param bibliographicalCitation
   * @return
   */

  def exampleURIGenerate(example: String, bibliographicalCitation: Option[String], dictionnaireUriPrefix: Option[String]): URI = {
    val citationString = if (bibliographicalCitation.isDefined) bibliographicalCitation.get.mkString else ""
    URI.create(DDFDictionnaireUriFactory.lexicogUsageExample(s"$example ${citationString}", dictionnaireUriPrefix))
  }

  /**
   *
   * lexical sens Lifty Annotations
   *
   * @param lexicalEntryUri
   * @param lexicalSensUri
   * @return
   */

  def lexicalSensGenerate(lexicalEntryUri: URI, lexicalSensUri: URI, model: ModelBuilder)  = {
    model.subject(lexicalEntryUri.toString)
      .add("http://www.w3.org/ns/lemon/ontolex#sense", createIRI(lexicalSensUri.toString.trim))

    model.subject(lexicalSensUri.toString)
      .add(RDF.TYPE, createIRI("http://www.w3.org/ns/lemon/ontolex#LexicalSense"))

  }

  /**
   *
   * skos definition triples creation
   *
   * @param definition
   * @param lexicalSensUri
   * @return
   */

  def skosDefinitionGenerate(definition: String, wasDerivedFrom: Option[String], lexicalSensUri: URI, model: ModelBuilder) = {
    model.subject(lexicalSensUri.toString)
      .add("http://www.w3.org/2004/02/skos/core#definition",
        if (definition.trim.isEmpty || definition.trim == "") "pas de définition"
        else definitionValue(definition, wasDerivedFrom)
      )
  }

  def definitionValue(definition: String, wasDerivedFrom: Option[String]): String = {
    if (wasDerivedFrom.isDefined && DDFDictionnaireConfig.dico != "asom"){
       s"""${TextHelper.htmlIt(definition.trim)} <a href="${wasDerivedFrom.get}">(fiche originale)</a>"""
    }
    else TextHelper.htmlIt(definition.trim)
  }

  def definitionValue(definition: String) = {
    TextHelper.htmlIt(definition.trim)
  }

  def scientificName(scientificName: Option[String],lexicalSensUri: URI, model: ModelBuilder) = {
    if(scientificName.isDefined) {
      model.subject(lexicalSensUri.toString)
        .add("http://data.dictionnairedesfrancophones.org/ontology/ddf#hasScientificName", scientificName.get)
    }
    else model
  }

  /**
   *
   * usage example creation
   *
   * @param example
   * @param lexicalSensUri
   * @param exempleURI
   * @return
   */

  def usageExampleGenerate(example: String, lexicalSensUri: URI, exempleURI: URI, model: ModelBuilder)  = {
    model.subject(lexicalSensUri.toString)
      .add("http://www.w3.org/ns/lemon/lexicog#usageExample", createIRI(exempleURI.toString.trim))
    model.subject(exempleURI.toString)
      .add(RDF.TYPE, createIRI("http://www.w3.org/ns/lemon/lexicog#UsageExample"))
      .add("http://www.w3.org/1999/02/22-rdf-syntax-ns#value", TextHelper.htmlIt(example.trim))
  }

  /**
   *
   * bibliographical citation triples creation
   *
   * @param exempleURI
   * @param source
   * @return
   */

  def bibliographicalCitationGenerate(exempleURI: URI, source: String, model: ModelBuilder) = {
    model.subject(exempleURI.toString)
      .add("http://purl.org/dc/terms/bibliographicalCitation", (TextHelper.htmlIt(source.trim)))
  }

  /**
   *
   * triples creation from ddf skos concepts
   *
   * @param lexicalSensURI
   * @param vocsLexical
   * @return
   */

  def vocsLexical(lexicalSensURI: URI, vocsLexical: Seq[VocsLexical], model: ModelBuilder)  = {
    if (vocsLexical.size < 1) None
    else Some(vocsLexical.map { vocLexical =>
      vocLexical.content.split(";").foreach(content =>
        //LiftyAnnotation(lexicalSensURI, vocLexical.vocab, createIRI(content.trim), 1.0)
          model.subject(lexicalSensURI.toString)
            .add(vocLexical.vocab, createIRI(content.trim))
      )
    })
  }

  def vocsLexical(seqs :Seq[VocsLexical], voc: String) = {
    seqs.filter(_.vocab == voc).last.content
  }

  /**
   *
   * contribution triples creation
   *
   * @param lexicalSensURI
   * @param aboutSens
   * @return
   */

  def aboutSens(lexicalSensURI: URI, aboutSens: Option[String], model: ModelBuilder, dictionnaireUriPrefix: Option[String]) = {

    if (!aboutSens.isDefined || LexicalEntryRdfAnnotationsGenerator.isEmpty(aboutSens.get)) model
    else {
      val uriContribution = DDFDictionnaireUriFactory.ddfOnlineContribution(lexicalSensURI.toString, aboutSens.get, dictionnaireUriPrefix)

      model.subject(lexicalSensURI.toString)
        .add("http://data.dictionnairedesfrancophones.org/ontology/ddf#hasItemAboutSense", createIRI(uriContribution.trim))
      model.subject(uriContribution)
        .add(RDF.TYPE, createIRI("http://data.dictionnairedesfrancophones.org/ontology/ddf#OnlineContribution"))
        .add("http://rdfs.org/sioc/ns#content", aboutSens.get.trim)
      }
  }

  /**
   *
   * @param lexicalSensURI
   * @param hasLocalisation
   * @return
   */

  def lexicalSensLocation(lexicalSensURI: URI, hasLocalisation: Option[String], model: ModelBuilder) = {
    if (hasLocalisation.isDefined) {
      if (!LexicalEntryRdfAnnotationsGenerator.isEmpty(hasLocalisation.get)) {
        hasLocalisation.get.split("\\;").foreach { location =>
          model.subject(lexicalSensURI.toString)
            .add("http://data.dictionnairedesfrancophones.org/ontology/ddf#hasLocalisation", createIRI(location.trim))
        }
      }
    }
  }
}