package org.ddf.annotator.services.recognizer

import org.ddf.annotator.helpers.RDFSerializationUtil.createIRI

import java.net.URI
import org.ddf.annotator.helpers.{DDFDictionnaireUriFactory, TextHelper}
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

import scala.collection.mutable

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Lexical Annotations setter
 */

object LexicalEntryAnnotationsRecogs {

  /**
   * lexical entry triples creation
   * @param lexicogEntryUri
   * @param lexicalEntryUri
   * @return
   */

  def lexicalEntryAnnotations(lexicogEntryUri: URI, lexicalEntryUri: URI, model: ModelBuilder) = {
    model.subject(createIRI(lexicogEntryUri.toString))
      .add("http://www.w3.org/ns/lemon/lexicog#describes", createIRI(lexicalEntryUri.toString))

    //Seq(LiftyAnnotation(lexicogEntryUri, "http://www.w3.org/ns/lemon/lexicog#describes", createIRI(lexicalEntryUri.toString), 1.0))
  }

  /**
   *
   * form triples annotations
   *
   * @param lexicalEntryURI
   * @param pageTitle
   * @return
   */

  def formAnnotations(lexicalEntryURI:URI, pageTitle: String, aboutForm:  Option[String], model: ModelBuilder, dictionnaireUriPrefix:Option[String])  = {
    model.subject(createIRI(lexicalEntryURI.toString))
      .add("http://www.w3.org/ns/lemon/ontolex#canonicalForm", createIRI(DDFDictionnaireUriFactory.ontolexForm(pageTitle.trim, dictionnaireUriPrefix)))
      .add(RDF.TYPE, createIRI(extractClassForm(pageTitle)))

    model.subject(createIRI(DDFDictionnaireUriFactory.ontolexForm(pageTitle.trim,dictionnaireUriPrefix)))
      .add(RDF.TYPE, createIRI(s"http://www.w3.org/ns/lemon/ontolex#Form"))
      .add("http://www.w3.org/ns/lemon/ontolex#writtenRep", pageTitle.trim)

    aboutFormAnnotations(new URI(DDFDictionnaireUriFactory.ontolexForm(pageTitle.trim, dictionnaireUriPrefix)), aboutForm, model, dictionnaireUriPrefix)
  }


  private def aboutFormAnnotations(formURI: URI, aboutForm: Option[String], model: ModelBuilder, dictionnaireUriPrefix:Option[String]) = {
    if (!aboutForm.isDefined) model
    else {
      val uriContribution = DDFDictionnaireUriFactory.ddfOnlineContribution(formURI.toString, aboutForm.get.trim, dictionnaireUriPrefix)
      model.subject(createIRI(formURI.toString))
        .add("http://data.dictionnairedesfrancophones.org/ontology/ddf#hasItemAboutForm", createIRI(uriContribution))

      model.subject(createIRI(URI.create(uriContribution).toString))
        .add(RDF.TYPE, createIRI("http://data.dictionnairedesfrancophones.org/ontology/ddf#OnlineContribution"))
        .add("http://rdfs.org/sioc/ns#content", aboutForm.get.trim)
    }
  }

  /**
   * type of entry
   *
   * @param pageTitle
   * @return
   */

  def extractClassForm(pageTitle: String): String = {
    val split1 = pageTitle.trim.split(" ").size
    val split2 = pageTitle.trim.split("-").size
    val split3 = pageTitle.trim.split("'").size
    if (split1 > 1 | split2 > 1 | split3 > 1) "http://www.w3.org/ns/lemon/ontolex#MultiWordExpression" else "http://www.w3.org/ns/lemon/ontolex#Word"
  }

  /**
   * other form triples creation
   *
   * @param words
   * @param lexicalEntryURI
   * @return
   */

  def otherFormAnnotations(words: Seq[String], lexicalEntryURI:URI, model: ModelBuilder, dictionnaireUriPrefix:Option[String]) = {
    words.filterNot(word => word.trim.isEmpty || (word.trim == "")).foreach { word =>
      val otherFormURI = DDFDictionnaireUriFactory.ontolexForm(word, dictionnaireUriPrefix)

      model.subject(lexicalEntryURI.toString)
        .add("http://www.w3.org/ns/lemon/ontolex#otherForm", createIRI(otherFormURI))

      model.subject(otherFormURI)
        .add(RDF.TYPE, createIRI(s"http://www.w3.org/ns/lemon/ontolex#Form"))
        .add("http://www.w3.org/ns/lemon/ontolex#writtenRep", s"${word.trim}")
    }
  }

  /**
   *
   * pronounciation triples creation
   *
   * @param lexicalEntryURI
   * @param phonetiques
   * @return
   */

  def pronounciationAnnotations(wordURI: URI, phonetiques: Seq[String], model: ModelBuilder)  = {
    phonetiques.filterNot(word => word.trim.isEmpty || (word.trim == "")).foreach { phonetique =>
      model.subject(wordURI.toString)
        .add("http://www.w3.org/ns/lemon/ontolex#phoneticRep", phonetique.trim)
    }
  }

  /**
   * etymology triples creation
   *
   * @param lexicalEntryURI
   * @param etymologyText
   * @return
   */

  def etymologyAnnotations(lexicalEntryURI: URI, etymologyText:String, model: ModelBuilder, dictionnaireUriPrefix:Option[String]) = {
      val uriEtymology: URI = URI.create(DDFDictionnaireUriFactory.siocPost(etymologyText, dictionnaireUriPrefix))

    model.subject(lexicalEntryURI.toString)
      .add(s"http://data.dictionnairedesfrancophones.org/ontology/ddf#hasItemAboutEtymology", createIRI(uriEtymology.toString.trim))
    model.subject(uriEtymology.toString)
      .add(RDF.TYPE, createIRI(s"http://data.dictionnairedesfrancophones.org/ontology/ddf#OnlineContribution"))
      .add("http://rdfs.org/sioc/ns#content", TextHelper.htmlIt(etymologyText.trim))
      .add("http://data.dictionnairedesfrancophones.org/ontology/ddf#aboutEtymology", createIRI(lexicalEntryURI.toString))
  }

  /*
    /**
     * localisation for form triples creation
     *
     * @param lexicalEntryURI
     * @param formLocalisation
     * @return
     */

    def localisationForForm(lexicalEntryURI: URI, formLocalisation: Option[String]) = {
      LiftyAnnotation(lexicalEntryURI, "http://data.dictionnairedesfrancophones.org/ontology/ddf#formHasLocalisation", formLocalisation.get, 1.0)
    }
  */
  /**
   * multi word triples creation
   *
   * @param lexicalEntryURI
   * @param multiWordType
   * @return
   */

  def multiWord(lexicalEntryURI: URI, multiWordType: Option[String], model: ModelBuilder) = {
    model.subject(lexicalEntryURI.toString)
      .add("http://data.dictionnairedesfrancophones.org/ontology/ddf#multiWordType", createIRI(multiWordType.get.trim))
  }

  /**
   *
   * gender triples creation
   *
   * @param lexicalEntryURI
   * @param hasGender
   * @return
   */

  def gender(lexicalEntryURI: URI, hasGender: Option[String], model: ModelBuilder) = {
    model.subject(lexicalEntryURI.toString)
      .add("http://data.dictionnairedesfrancophones.org/ontology/ddf#hasGender", createIRI(hasGender.get.trim))
  }

  /**
   *
   * number triples creation
   *
   * @param lexicalEntryURI
   * @param hasNumber
   * @return
   */

  def number(lexicalEntryURI: URI, hasNumber: Option[String], model: ModelBuilder) = {
    model.subject(lexicalEntryURI.toString)
      .add("http://data.dictionnairedesfrancophones.org/ontology/ddf#hasNumber", createIRI(hasNumber.get.trim))
  }

  /**
   *
   * transitivity triples creation
   *
   * @param lexicalEntryURI
   * @param hasTransitivity
   * @return
   */

  def transitivity(lexicalEntryURI: URI, hasTransitivity: Option[String], model: ModelBuilder) = {
    model.subject(lexicalEntryURI.toString)
      .add("http://data.dictionnairedesfrancophones.org/ontology/ddf#hasTransitivity", createIRI(hasTransitivity.get.trim))
  }

  /**
   *
   * part of speech triples creation
   *
   * @param lexicalEntryURI
   * @param partOfSpeech
   * @param posType
   * @return
   */

  def pos(lexicalEntryURI: URI, partOfSpeech: Option[String], posType: mutable.Map[String, String], model: ModelBuilder) = {
    model.subject(lexicalEntryURI.toString)
      .add("http://data.dictionnairedesfrancophones.org/ontology/ddf#hasPartOfSpeech", createIRI(partOfSpeech.get))
    wordTypeAnnotations(lexicalEntryURI, partOfSpeech.get, posType, model)

  }

  /**
   *
   * form location triples creation
   *
   * @param lexicalEntryURI
   * @param locations
   * @return
   */

  def formHasLocalisation(lexicalEntryURI: URI, locations: Option[String], model: ModelBuilder)  = {
    locations.get.split("\\;").foreach { location =>
      model.subject(lexicalEntryURI.toString)
        .add("http://data.dictionnairedesfrancophones.org/ontology/ddf#formHasLocalisation", createIRI(location.trim))
    }
  }

  def wordTypeAnnotations(lexicalEntryURI: URI, partOfSpeech: String, posType: mutable.Map[String, String], model: ModelBuilder) = {
    //println("THE TYPE " + posType.get(partOfSpeech.concept))
    val postType = posType.get(partOfSpeech)
    val rs = if (postType.isDefined) {
      if (postType.get == "variable") {
        model.subject(lexicalEntryURI.toString)
          .add(RDF.TYPE, createIRI(s"http://data.dictionnairedesfrancophones.org/ontology/ddf#InflectablePOS"))
      }
      else if (postType.get == "verbe") {
        model.subject(lexicalEntryURI.toString)
          .add(RDF.TYPE,  createIRI(s"http://data.dictionnairedesfrancophones.org/ontology/ddf#Verb"))
      }
      else {
        model.subject(lexicalEntryURI.toString)
          .add(RDF.TYPE,  createIRI(s"http://data.dictionnairedesfrancophones.org/ontology/ddf#InvariablePOS"))
      }
    }
  }
}