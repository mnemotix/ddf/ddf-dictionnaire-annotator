package org.ddf.annotator.services

import java.io.File

import akka.Done
import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, ClosedShape}
import akka.stream.scaladsl.{Broadcast, GraphDSL, RunnableGraph, Sink}
import org.ddf.annotator.helpers.DDFDictionnaireConfig
import org.ddf.annotator.model.Entry

import scala.collection.mutable
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * The service that launch the annotations of a dictionary
 *
 * @param filePath path of the csv fiel
 */

// todo [[ ]] to <a href>

class DDFDictionnaireAnnotator(filePath: String, dicoName: Option[String] = None, dictionnaireUriPrefix: Option[String]= None, uriDictionnaire:Option[String]=None, graphUri:Option[String]= None) {

  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  implicit val system: ActorSystem = ActorSystem("AnnotationSystem")
  implicit val rdfReader = new DDFDictionnaireRdfReader()
  rdfReader.init
  val vocs: mutable.ListBuffer[(String, String)] = rdfReader.vocsBlocked
  val posType: mutable.Map[String, String] = rdfReader.posTypeBlocked
  val senseRelation: mutable.Seq[String] = rdfReader.senseRelationBlocked

  lazy val extractor = new DDFDictionnaireExtractor(filePath, Some(','.toByte))
  lazy val setter = new DDFDictionnaireModelSetter(posType, senseRelation, vocs, dictionnaireUriPrefix, dicoName.getOrElse(s"${DDFDictionnaireConfig.dico}"))
  lazy val recognizer = new DDFDictionnaireRecognizer(posType, dictionnaireUriPrefix, uriDictionnaire, graphUri)
  lazy val cacheManager = new DDFDictionnaireDBCacheManager()

  lazy val dumper = new DDFDictionnaireFileOutput(dicoName = dicoName, graphUri = graphUri)

  lazy val sink = Sink.ignore
  lazy val sinkBottom = Sink.ignore
  lazy val cache = DDFDictionnaireDBCacheManager.initDB(dicoName.getOrElse(s"${DDFDictionnaireConfig.dico}"))

  /**
   *
   * Create the files where to dump the files
   *
   * @return
   */

  def init = {
    new File(DDFDictionnaireConfig.ddfDir).mkdir()
    dumper.init
  }

  def annotator: (Future[Done], Future[Done]) = {
    cache.init
    RunnableGraph.fromGraph(GraphDSL.create(sink, sinkBottom)((_,_)) { implicit builder =>
      (s, b) =>
      import GraphDSL.Implicits._

      val bcast = builder.add(Broadcast[Entry](2))

      extractor.extract ~> setter.setter ~> bcast ~>  recognizer.recognize.grouped(100000) ~> dumper.dump ~> s.in
        bcast ~> cacheManager.transform(DDFDictionnaireDBCacheManager.transformerLexicogEntry) ~> cacheManager.bulkDump(cache) ~> b.in
      ClosedShape
    }).run()
  }

  def shutdown: Unit = {
    rdfReader.shutdown()
    cache.shutdown()
    system.terminate()
  }
}
