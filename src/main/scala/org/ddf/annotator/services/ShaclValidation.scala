/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.annotator.services

import akka.actor.ActorSystem
import com.mnemotix.analytix.commons.utils.FileUtils
import com.mnemotix.synaptix.core.MonitoredService
import com.mnemotix.synaptix.core.utils.StringUtils
import org.ddf.annotator.DDFShaclException
import org.ddf.annotator.services.shacl.DictValidationShape
import org.eclipse.rdf4j.exceptions.ValidationException
import org.eclipse.rdf4j.model.vocabulary.RDF4J
import org.eclipse.rdf4j.repository.sail.SailRepository
import org.eclipse.rdf4j.rio.{RDFFormat, Rio}
import org.eclipse.rdf4j.sail.memory.MemoryStore
import org.eclipse.rdf4j.sail.shacl.ShaclSail

import java.io.{File, FileOutputStream, StringReader}

class ShaclValidation(rdfFilesPath: String)(implicit  system: ActorSystem) extends MonitoredService {

  lazy val shaclSail = new ShaclSail(new MemoryStore())
  lazy val sailRepository = new SailRepository(shaclSail)


  def validate(): Unit = {
    loadRules()
    val path = StringUtils.removeTrailingSlashes(rdfFilesPath)
    val files = FileUtils.getListOfFiles(new File(s"${path}"), List("trig"))
    files.foreach { file =>
      validateResult(file)
    }
  }

  def loadRules(): Unit = {
    try {
      val connection = sailRepository.getConnection()
      connection.begin()
      val shaclRules = new StringReader(
        DictValidationShape.shacl
      )
      connection.add(shaclRules, "", RDFFormat.TURTLE, RDF4J.SHACL_SHAPE_GRAPH)
      connection.commit()
      connection.close()
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the loading of shacl rules", t)
        throw new DDFShaclException("An error occured during the loading of shacl rules", Some(t))
    }
  }

  def validateResult(file: File) = {
    val path = StringUtils.removeTrailingSlashes(rdfFilesPath)
    val fileOutput =  new FileOutputStream(s"${path}/error.trig")
    try {
      val connection = sailRepository.getConnection()
      connection.begin()
      connection.add(file)
      connection.commit()

    }
    catch {
      case t: Throwable => {
        val cause = t.getCause
        if (cause.isInstanceOf[ValidationException]) {
          val model = cause.asInstanceOf[ValidationException].validationReportAsModel()
          Rio.write(model, fileOutput, RDFFormat.TURTLE)
          logger.error(s"Shacl error", t)
          throw t
        }
        else {
          logger.error(s"An error occured during the shacl validation", t)
          throw new DDFShaclException("An error occured during the shacl validation", Some(t))
        }

      }
    }
  }

  override val serviceName: String = "Shacl Validation"

  override def init(): Unit = {
    sailRepository.init()
  }

  override def shutdown(): Unit = {
    sailRepository.shutDown()
  }
}