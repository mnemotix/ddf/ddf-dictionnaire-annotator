package org.ddf.annotator.services

import java.io.{File, FileOutputStream, IOException}
import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.synaptix.core.MonitoredService
import com.typesafe.scalalogging.LazyLogging
import org.ddf.annotator.DDFOutputException
import org.ddf.annotator.helpers.{DDFDictionnaireConfig, RDFSerializationUtil}
import org.eclipse.rdf4j.model.impl.SimpleValueFactory
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.rio.{RDFFormat, RDFHandlerException, Rio, UnsupportedRDFormatException}

import java.util.concurrent.atomic.AtomicInteger

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * the sercvice that write the triples to files
 */

class DDFDictionnaireFileOutput(dicoName: Option[String] = None, graphUri: Option[String]=None, isSenseRelation: Boolean = false) extends LazyLogging with MonitoredService {
  lazy val dir = s"${DDFDictionnaireConfig.ddfDir}"
  lazy val contextIRI = SimpleValueFactory.getInstance().createIRI(graphUri.getOrElse(s"${DDFDictionnaireConfig.graphUri}"))

  val fileOutGenerator: String => FileOutputStream = (name:String) => {
    if (isSenseRelation) {
      new FileOutputStream(s"${dir}/${dicoName.getOrElse(s"${DDFDictionnaireConfig.dico}")}/rdf/${name}_sr.trig")
    }
    else new FileOutputStream(s"${dir}/${dicoName.getOrElse(s"${DDFDictionnaireConfig.dico}")}/rdf/$name.trig")
  }
  val part = new AtomicInteger(0)
  val i = new AtomicInteger(0)

  /**
   *
   * create the directory where the files will be dump
   *
   * @return
   */

  def init(): Unit = {
    try {
      new File(s"$dir/${dicoName.getOrElse(DDFDictionnaireConfig.dico)}").mkdirs()
      new File(s"$dir/${dicoName.getOrElse(DDFDictionnaireConfig.dico)}/rdf").mkdirs()

    }
    catch {
      case t: IOException => throw t
      case t: IllegalArgumentException => throw t
      case t: SecurityException =>  throw t
    }
  }

  /**
   *
   * the dumping method that write a number of triples on a file
   *
   * @return
   */

  def dump: Flow[Seq[Model], Unit, NotUsed] = {
    Flow[Seq[Model]].map { recogs =>
      try {
        val nwPart = part.incrementAndGet()
        val name = s"$nwPart-${dicoName.getOrElse(DDFDictionnaireConfig.dico)}"
        val file = fileOutGenerator(name)
        val model = RDFSerializationUtil.merge(recogs)
        Rio.write(model, file, RDFFormat.TRIG)
        logger.info(s"Dump ${recogs.size} rdf models in $name")
        val j = i.addAndGet(recogs.size)
        notifyProgress(j, None, Some(s"Dump ${recogs.size} rdf models in $name"))
      }
      catch {
        case t: RDFHandlerException =>  throw new RDFHandlerException("RDF Handler Exception", t)
        case t: UnsupportedRDFormatException =>  throw new UnsupportedRDFormatException("RDF Format Exception", t)
        case t: Throwable => throw new DDFOutputException("An error occured during the output process", Some(t))
      }
    }
  }

  /**
   *
   * set nameSpace
   *
   * @param builder
   * @return
   */

  def nameSpace(builder: ModelBuilder) = {
    builder.setNamespace("wik", "http://data.dictionnairedesfrancophones.org/resource/wiktionnaire")
      .setNamespace("owl", "http://www.w3.org/2002/07/owl#")
      .setNamespace("ddf","http://data.dictionnairedesfrancophones.org/ontology/ddf#")
      .setNamespace("xsd", "http://www.w3.org/2001/XMLSchema#")
      .setNamespace("rdfs", "http://www.w3.org/2000/01/rdf-schema#")
      .setNamespace("lexinfo", "http://www.lexinfo.net/ontology/2.0/lexinfo#")
      .setNamespace("core", "http://www.w3.org/2004/02/skos/core#")
      .setNamespace("lemon", "http://lemon-model.net/lemon#")
      .setNamespace("dct", "http://purl.org/dc/terms/")
      .setNamespace("rdf","http://www.w3.org/1999/02/22-rdf-syntax-ns#")
      .setNamespace("xml", "http://www.w3.org/XML/1998/namespace")
      .setNamespace("lexicog", "http://www.w3.org/ns/lemon/lexicog#")
      .setNamespace("ontolex", "http://www.w3.org/ns/lemon/ontolex#")
      .setNamespace("prov", "http://www.w3.org/ns/prov-o-20130430#")
      .setNamespace("sioc", "http://rdfs.org/sioc/ns#").build()
  }

  override val serviceName: String = "DDFDictionnaireFileOutput"

  override def shutdown(): Unit = {}
}