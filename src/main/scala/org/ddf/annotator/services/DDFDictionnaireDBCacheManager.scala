package org.ddf.annotator.services

import java.io.File
import java.nio.charset.StandardCharsets

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.synaptix.cache.RocksDBStore
import com.typesafe.scalalogging.LazyLogging
import org.ddf.annotator.DDFCacheException
import org.ddf.annotator.helpers.DDFDictionnaireConfig
import org.ddf.annotator.model.Entry

import scala.concurrent.{ExecutionContext}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class DDFDictionnaireDBCacheManager(implicit ec: ExecutionContext) extends LazyLogging  {
  val UTF8: String = StandardCharsets.UTF_8.name

  def bulkDump(store: RocksDBStore): Flow[(String, Array[Byte]),Unit , NotUsed] = {
    try {
      Flow[(String, Array[Byte])].map { couples =>
        val p = store.put(couples._1, couples._2)

      }
    }
      /*
      Flow[(Array[Byte], ByteString)].grouped(500) { couples =>
        store.put(couples._1, couples._2)
      }
      */
    catch {
      case t: Throwable =>
        logger.error("An error occured during the caching process", t)
        throw new DDFCacheException("An error occured during the caching process", Some(t))
    }
  }

  def transform(transformer: Entry => ((String, Array[Byte]))): Flow[Entry, (String, Array[Byte]), NotUsed] = {
    Flow[Entry].map { entry =>
      transformer(entry)
    }
  }

  def filterCachedEntry(store: RocksDBStore): Flow[Entry, Entry, NotUsed] = {
    Flow[Entry].filter(entry => DDFDictionnaireDBCacheManager.filterEntryCache(entry, store))
  }

  def filterChangedEntry(store: RocksDBStore): Flow[Entry, Entry, NotUsed] = {
    Flow[Entry].filter(entry => DDFDictionnaireDBCacheManager.filterEntryChanged(entry, store))
  }

  def updateCachedEntry(store: RocksDBStore): Flow[Entry, Unit, NotUsed] = {
    Flow[Entry].map { entry =>
      DDFDictionnaireDBCacheManager.updateEntry(entry, store)
    }
  }

}

object DDFDictionnaireDBCacheManager {
  lazy val UTF8: String = StandardCharsets.UTF_8.name

  lazy val transformerLexicogEntry: Entry => (String, Array[Byte]) = (entry: Entry) => {
    toByte((entry.form, entry.uri.toString))
  }

  lazy val transformerCacheEntry: Entry => (String, Array[Byte]) = (entry: Entry) => {
    toByte((entry.uri.toString, entry.lentries.toString))
  }

  def initTmpDB(tmpDBName:  String) = {
    val tmpFile = File.createTempFile(tmpDBName, ".db")
    val tmpFileName = tmpFile.getAbsolutePath
    tmpFile.delete
    new RocksDBStore(tmpFileName)
  }

  def initDB(DBName: String) = {
    new RocksDBStore(s"${DDFDictionnaireConfig.ddfDir}/$DBName.db")
  }

  def toByte(entries: (String, String)): (String, Array[Byte]) = {
    (entries._1, (entries._2.getBytes(UTF8)))
  }

  def filterEntryCache(entry: Entry, store: RocksDBStore)(implicit ec: ExecutionContext): Boolean = {
    val result = store.get(entry.uri.toString)
    val stringEntry = (entry.text.getBytes(UTF8))

    if (result.isDefined) {
      !result.get.equals(stringEntry)
    }
    else true
  }

  def filterEntryChanged(entry: Entry, store: RocksDBStore)(implicit ec: ExecutionContext): Boolean = {
    val result = store.get(entry.form)
    result.isDefined
  }

  def updateEntry(entry: Entry, store: RocksDBStore)(implicit ec: ExecutionContext) = {
    val future = store.delete(entry.uri.toString)
    if (future) {
      store.put(entry.uri.toString, (entry.text.getBytes(UTF8)))
    }
    else false
  }
}