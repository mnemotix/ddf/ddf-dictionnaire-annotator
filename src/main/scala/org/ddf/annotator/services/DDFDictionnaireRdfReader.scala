package org.ddf.annotator.services

import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.{BindingSetIterator, Done}
import com.typesafe.scalalogging.LazyLogging
import org.apache.commons.compress.archivers.zip.ZipUtil
import org.ddf.annotator.helpers.{ContribExportConfig, DDFDictionnaireConfig}
import org.eclipse.rdf4j.model.Statement
import org.eclipse.rdf4j.model.impl.SimpleValueFactory
import org.eclipse.rdf4j.repository.RepositoryResult
import org.eclipse.rdf4j.rio.{RDFFormat, Rio}

import java.io.{File, FileOutputStream}
import java.time.{LocalDateTime, ZoneId, ZoneOffset}
import java.time.format.DateTimeFormatter
import scala.collection.mutable
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.reflect.io.Directory
import scala.util.{Failure, Success}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * the service that connect to a triplestore to get informations about the model
 * @param ec
 */

class DDFDictionnaireRdfReader(implicit val ec: ExecutionContext) extends LazyLogging {

  implicit lazy val conn = RDFClient.getReadConnection((DDFDictionnaireConfig.repositoryName))
  implicit lazy val writeConn = RDFClient.getWriteConnection((DDFDictionnaireConfig.repositoryName))

  def shutdown() = {
    conn.close()
    writeConn.close()
  }

  lazy val posTypeQuery =
    """PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
      |PREFIX dct: <http://purl.org/dc/terms/>
      |SELECT ?concept ?type
      |WHERE {
      | <http://data.dictionnairedesfrancophones.org/authority/part-of-speech-type> a skos:ConceptScheme .
      | ?concept skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/part-of-speech-type> .
      | ?concept dct:type ?type .
      |}
    """.stripMargin

  lazy val skosVocsQuery =
    """prefix skos: <http://www.w3.org/2004/02/skos/core#>
      |SELECT ?voc
      |WHERE {
      | ?voc a skos:ConceptScheme .
      |}""".stripMargin

  lazy val senseRelationQuery =
    """PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
      |PREFIX dct: <http://purl.org/dc/terms/>
      |SELECT ?concept
      |WHERE {
      | <http://data.dictionnairedesfrancophones.org/authority/sense-relation> a skos:ConceptScheme .
      | ?concept skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/sense-relation> .
      |}""".stripMargin

  lazy val counterClasses =
     s"""
      |PREFIX lexicog: <http://www.w3.org/ns/lemon/lexicog#>
      |PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
      |SELECT ?type (COUNT(?s) as ?nbInstances)
      |WHERE {
      |    GRAPH <${DDFDictionnaireConfig.graphUri}>{
      |        VALUES ?type {lexicog:Entry ontolex:Form ontolex:LexicalSense lexicog:UsageExample }
      |        ?s a ?type .
      |    }
      |}
      |GROUP BY ?type""".stripMargin

  lazy val deleteGraphQuery: String => String = (x: String) => s"DROP GRAPH <$x>"

  def dropGraph(x: String) = {
      RDFClient.update(s"DROP GRAPH <$x>")
  }

  def init: Unit = RDFClient.init()

  def senseRelationAsync: Future[mutable.ListBuffer[String]] = {
    var posType = scala.collection.mutable.ListBuffer[String]()
    val futureTypeAsync = RDFClient.select(senseRelationQuery).map { res =>
      val tupleQueryResult = res.resultSet
      while (tupleQueryResult.hasNext) {
        val bs = tupleQueryResult.next()
        posType += bs.getValue("concept").stringValue()
      }
      posType
    }

    futureTypeAsync onComplete {
      case Success(i) => logger.info(s"The number of Pos Type : ${i.size}")
      case Failure(t) => logger.error("There is a failure here", t)
    }
    futureTypeAsync
  }

  def senseRelationBlocked: mutable.Seq[String] = {
    val futurePos = senseRelationAsync
    Await.result(futurePos, Duration.Inf)
  }

  def posTypeAsync: Future[mutable.Map[String, String]] = {
    var posType = scala.collection.mutable.Map[String, String]()
    val futureTypeAsync = RDFClient.select(posTypeQuery).map { res =>
      val tupleQueryResult: BindingSetIterator = res.resultSet
      while (tupleQueryResult.hasNext) {
        val bs = tupleQueryResult.next()
        posType += (bs.getValue("concept").stringValue() -> bs.getValue("type").stringValue())
      }
      posType
    }

    futureTypeAsync onComplete {
      case Success(i) => logger.info(s"The number of Pos Type : ${i.size}")
      case Failure(t) => logger.error("There is a failure here", t)
    }
    futureTypeAsync
  }

  def posTypeBlocked: mutable.Map[String, String] = {
    val futurePos = posTypeAsync
    Await.result(futurePos, Duration.Inf)
  }

  def vocsAsync: Future[mutable.ListBuffer[(String, String)]] = {
    var voc = scala.collection.mutable.ListBuffer[(String, String)]()
    val futureVocAsync = RDFClient.select(skosVocsQuery).map { res =>
      val tupleQueryResult = res.resultSet
      while (tupleQueryResult.hasNext) {
        val bs = tupleQueryResult.next()
        val tuples = vocabularyEntity(bs.getValue("voc").stringValue())
        voc += tuples
      }
      voc
    }

    futureVocAsync onComplete {
      case Success(i) => logger.info(s"The number of Pos Type : ${i.size}")
      case Failure(t) => logger.error("There is a failure here", t)
    }
    futureVocAsync
  }

  def countClasses = {
    val resultat = scala.collection.mutable.ListBuffer[(String, String)]()



  }

  def vocsBlocked: mutable.ListBuffer[(String, String)] = {
    val futurePos = vocsAsync
    Await.result(futurePos, Duration.Inf)
  }

  def getContribs(dicoName: String): Unit = {
    lazy val format = "yyyyMMdd"
    lazy val dtf = DateTimeFormatter.ofPattern(format)
    lazy val now = LocalDateTime.now()
    lazy val zone = ZoneId.of("Europe/Paris")
    lazy val dirName = ContribExportConfig.csvOutputDir
    new File(s"${dirName}/${now.format(dtf)}_${dicoName}").mkdirs()
    lazy val fileOutputStream = new FileOutputStream(s"${dirName}${now.format(dtf)}_${dicoName}/${dicoName}_${now.toEpochSecond(ZoneOffset.of("+02:00")).toString}.trig")
    val graph = SimpleValueFactory.getInstance().createIRI(s"http://data.dictionnairedesfrancophones.org/dict/$dicoName/graph")
    val statements: RepositoryResult[Statement] = conn.conn.getStatements(null, null, null, false, graph)
    Rio.write(statements, fileOutputStream, RDFFormat.TRIG)
    fileOutputStream.close()
    org.zeroturnaround.zip.ZipUtil.pack(new File(s"${dirName}${now.format(dtf)}_${dicoName}"), new File(s"${dirName}${now.format(dtf)}_${dicoName}.zip"))
    val del = new Directory(new File(s"${dirName}${now.format(dtf)}_${dicoName}/")).deleteRecursively()
  }

  /*
    def vocabulariesAsync: Future[ListBuffer[VocabularyEntity]] = {
    var vocabularies = new ListBuffer[VocabularyEntity]()
    val futureVocabulariesAsync = RDFClient.select(vocabularySparqlQuery).map { res =>
      val tupleQueryResult = res.resultSet.right.get
      while (tupleQueryResult.hasNext) {
        val bs = tupleQueryResult.next()
        val vocEntity = vocabularyEntity(bs)
        vocabularies += vocEntity
      }
      vocabularies
    }

    futureVocabulariesAsync onComplete {
      case Success(i) => logger.info(s"The number of vocabulaire : ${i.size}")
      case Failure(t) => logger.error("There is a failure here", t)
    }
    futureVocabulariesAsync
  }
   */

  def vocabularyEntity(voc: String): (String,String) = {
    voc match {
      case "http://data.dictionnairedesfrancophones.org/authority/connotation" | "ddfa:connotation" => ("ddf:hasConnotation", "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasConnotation")
      case "http://data.dictionnairedesfrancophones.org/authority/domain" | "ddfa:domain"  => ("ddf:hasDomain", "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasDomain")
      case "http://data.dictionnairedesfrancophones.org/authority/form-type" | "ddfa:form-type"  => ("ddf:hasFormType","http://data.dictionnairedesfrancophones.org/ontology/ddf#hasFormType")
      case "http://data.dictionnairedesfrancophones.org/authority/frequency" | "ddfa:frequency" => ("ddf:hasFrequency","http://data.dictionnairedesfrancophones.org/ontology/ddf#hasFrequency")
      //case "http://data.dictionnairedesfrancophones.org/authority/gender" | "ddfa:gender" => ("ddf:hasGender","http://data.dictionnairedesfrancophones.org/ontology/ddf#hasGender")
      case "http://data.dictionnairedesfrancophones.org/authority/glossary" | "ddfa:glossary" => ("ddf:hasGlossary" ,"http://data.dictionnairedesfrancophones.org/ontology/ddf#hasGlossary")
      case "http://data.dictionnairedesfrancophones.org/authority/grammatical-constraint" | "ddfa:grammatical-constraint" => ("ddf:hasGrammaticalConstraint","http://data.dictionnairedesfrancophones.org/ontology/ddf#hasGrammaticalConstraint")
      case "http://data.dictionnairedesfrancophones.org/authority/mood" | "ddfa:mood" => ("ddf:hasMood" ,"http://data.dictionnairedesfrancophones.org/ontology/ddf#hasMood")

      //case "http://data.dictionnairedesfrancophones.org/authority/multiword-type" | "ddfa:multiword-type"=> ("ddf:multiWordType", "http://data.dictionnairedesfrancophones.org/ontology/ddf#multiWordType")
      //case "http://data.dictionnairedesfrancophones.org/authority/number" | "ddfa:number" => ("ddf:hasNumber" ,"http://data.dictionnairedesfrancophones.org/ontology/ddf#hasNumber")
      //case "http://data.dictionnairedesfrancophones.org/authority/part-of-speech-type" | "ddfa:part-of-speech-type" => ("ddf:hasPartOfSpeech","http://data.dictionnairedesfrancophones.org/ontology/ddf#hasPartOfSpeech")
      case "http://data.dictionnairedesfrancophones.org/authority/person" | "ddfa:person" =>  ("ddf:hasPerson","http://data.dictionnairedesfrancophones.org/ontology/ddf#hasPerson")

      case "http://data.dictionnairedesfrancophones.org/authority/place" | "ddfa:place" => ("ddf:hasLocalisation","http://data.dictionnairedesfrancophones.org/ontology/ddf#hasLocalisation")
      case "http://data.dictionnairedesfrancophones.org/authority/register" | "ddfa:register" =>  ("ddf:hasRegister" ,"http://data.dictionnairedesfrancophones.org/ontology/ddf#hasRegister")
      case "http://data.dictionnairedesfrancophones.org/authority/sense-relation" | "ddfa:sense-relation" => ("ddf:hasSemanticRelationType" ,"http://data.dictionnairedesfrancophones.org/ontology/ddf#hasSemanticRelationType")
      case "http://data.dictionnairedesfrancophones.org/authority/sociolect" | "ddfa:sociolect" =>  ("ddf:hasSociolect" ,"http://data.dictionnairedesfrancophones.org/ontology/ddf#hasSociolect")

      case "http://data.dictionnairedesfrancophones.org/authority/temporality" | "ddfa:temporality"  => ("ddf:hasTemporality","http://data.dictionnairedesfrancophones.org/ontology/ddf#hasTemporality")
      case "http://data.dictionnairedesfrancophones.org/authority/tense" | "ddfa:tense" => ("ddf:hasTense","http://data.dictionnairedesfrancophones.org/ontology/ddf#hasTense")
      case "http://data.dictionnairedesfrancophones.org/authority/term-element" | "ddfa:term-element" =>  ("lexinfo:termElement","http://www.lexinfo.net/ontology/2.0/lexinfo#termElement")
      case "http://data.dictionnairedesfrancophones.org/authority/textual-genre" | "ddfa:textual-genre" => ("ddf:hasTextualGenre","http://data.dictionnairedesfrancophones.org/ontology/ddf#hasTextualGenre")
      //case "http://data.dictionnairedesfrancophones.org/authority/verb-frame" | "ddfa:verb-frame" => ("ddf:hasTransitivity", "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasTransitivity")
      case _ => ("", "")
    }
  }
}
