package org.ddf.annotator.services

import java.nio.charset.StandardCharsets
import java.nio.file.Paths
import akka.stream.alpakka.csv.scaladsl.{CsvParsing, CsvToMap}
import akka.stream.scaladsl.{FileIO, Source}
import akka.stream.{ActorMaterializer, IOResult}
import com.typesafe.scalalogging.LazyLogging
import org.ddf.annotator.DDFExtractorException

import scala.concurrent.Future

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * the service that read the csv file
 *
 * @param filePath
 * @param mat
 */

class DDFDictionnaireExtractor(filePath: String, delim: Option[Byte]) extends LazyLogging {

  def extract: Source[Map[String, String], Future[IOResult]] = {
    try {
      FileIO.fromPath(Paths.get(filePath))
        .via(CsvParsing.lineScanner(delimiter = delim.getOrElse(',') , maximumLineLength = 100 * 1024))
        .via(CsvToMap.toMapAsStrings(StandardCharsets.UTF_8))
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the resource extraction process", t)
        throw new DDFExtractorException("An error occured during the resource extraction process", Some(t))
    }
  }
}