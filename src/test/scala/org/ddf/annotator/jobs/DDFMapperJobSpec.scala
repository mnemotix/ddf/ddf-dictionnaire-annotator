/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.annotator.jobs

import akka.Done
import org.ddf.annotator.DDFDictionnaireAnnotatorTestSpec
import org.scalatest.time.SpanSugar.convertIntToGrainOfTime

import scala.concurrent.Await
import scala.util.{Failure, Success}

class DDFMapperJobSpec extends DDFDictionnaireAnnotatorTestSpec{

  it should "launch a mapping" in {
    val job = new DDFMapperJob()
    val future = job.execute()
    future.onComplete{
      case Success(value:Done) => value.isInstanceOf[Done] shouldBe true
      case Failure(_) => assert(false)
    }
    Await.result(future, 20.minutes)
    Thread.sleep(5000)
  }
}