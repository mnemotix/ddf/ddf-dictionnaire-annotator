package org.ddf.annotator.helpers

import com.mnemotix.synaptix.rdf.client.RDFClientConfiguration
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class DDFDictionnaireConfigSpec extends AnyFlatSpec with Matchers {

  behavior of "DDFDictionnaireConfig"

  it should "find the config of ddfDir" in {
    DDFDictionnaireConfig.ddfDir shouldBe "src/test/resources/data/"
  }

  it should "find the config of ddfName" in {
    DDFDictionnaireConfig.ddfLocation shouldBe "GrandDictionnaireTerminologique-Québec_liste-a-importer-dans-DDF-2020 - Liste.csv"
  }

  it should "find the config of the number of the triples" in {
    DDFDictionnaireConfig.triplesPerDumpFile shouldBe 800000
  }

  it should "find the dir of the rdf" in {
    DDFDictionnaireConfig.rdfDumpDir shouldBe "src/test/resources/data/rdf/"
  }

  it should "find the uri of the dictionnaire" in {
    DDFDictionnaireConfig.uriDictionnaire shouldBe "http://data.dictionnairedesfrancophones.org/dict/gdtoqlf"
  }

  it should "find the uri of the graph" in {
    DDFDictionnaireConfig.graphUri shouldBe "http://data.dictionnairedesfrancophones.org/dict/gdtoqlf/graph"
  }
}