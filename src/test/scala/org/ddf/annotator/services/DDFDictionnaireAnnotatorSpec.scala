package org.ddf.annotator.services

import java.io.File
import akka.Done
import org.ddf.annotator.helpers.DDFDictionnaireConfig
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class DDFDictionnaireAnnotatorSpec extends AnyFlatSpec with Matchers {

  implicit val executionContext =  ExecutionContext.global


  it should "annotate a dico" in {
    val path = new File(DDFDictionnaireConfig.ddfLocation)
    val annotator = new DDFDictionnaireAnnotator(path.getAbsolutePath)
    val fut: (Future[Done], Future[Done]) = annotator.annotator
    val result: Future[Seq[Done]] = Future.sequence(Seq(fut._1, fut._2))
    annotator.init

    result.onComplete {
      case Success(value) => {
        println(s"first passage is finished with value = $value")
        annotator.shutdown
      }
      case Failure(e) => {
        println(s"error : $e")
        e.printStackTrace
      }
    }
    val finalResult = Await.result(result, Duration.Inf)
    annotator.shutdown

   val annotator2 = new DDFDictionnaireSenseRelationAnnotator(path.getAbsolutePath)
    val future2 = annotator2.annotate
    future2.onComplete {
      case Success(value) => {
        println(s"second passage is finished with value = $value")
        annotator2.shutdown
      }
      case Failure(e) => e.printStackTrace
    }
    val finalResult2 = Await.result(future2, Duration.Inf)
    println(finalResult2)
    finalResult.size shouldEqual(2)

  }

  /*
  it should "annotate the Belgicismes" in {
    val path = new File("src/test/resources/data/Belgicismes-Belgicismes.csv")
    val annotator = new DDFDictionnaireAnnotator(path.getAbsolutePath)
    val fut = annotator.annotator
    annotator.init
    val  result = Await.result(fut, Duration.Inf)
    result.shouldEqual(Done)
  }
  */


}