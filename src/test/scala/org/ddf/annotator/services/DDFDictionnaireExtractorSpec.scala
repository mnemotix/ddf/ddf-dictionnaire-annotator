package org.ddf.annotator.services

import java.io.File

import akka.stream.IOResult
import akka.stream.scaladsl.{Sink, Source}
import org.ddf.annotator.DDFDictionnaireAnnotatorTestSpec
import org.ddf.annotator.helpers.DDFDictionnaireConfig

import scala.collection.immutable
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class DDFDictionnaireExtractorSpec extends DDFDictionnaireAnnotatorTestSpec {
  val path = new File("src/test/resources/data/test-GDT.csv")
  val extractor = new DDFDictionnaireExtractor(path.getAbsolutePath,None )

  it should "extract a map of line" in {
    val resource: Source[Map[String, String], Future[IOResult]] = extractor.extract
    val result: Future[immutable.Seq[Map[String, String]]] = resource.runWith(Sink.seq)
    val seq: immutable.Seq[Map[String, String]] = Await.result(result, Duration.Inf)
    println(seq)
    seq.size shouldEqual 8
  }
}