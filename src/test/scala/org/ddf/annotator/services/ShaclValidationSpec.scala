/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.annotator.services

import com.mnemotix.synaptix.core.utils.StringUtils
import org.ddf.annotator.DDFDictionnaireAnnotatorTestSpec
import org.ddf.annotator.helpers.DDFDictionnaireConfig

class ShaclValidationSpec extends DDFDictionnaireAnnotatorTestSpec {

  it should "validate some shacl file" in {
    val dicoName = DDFDictionnaireConfig.dico
    val rdfFilesPath = s"${DDFDictionnaireConfig.ddfDir}/$dicoName/rdf"
    val path = StringUtils.removeTrailingSlashes(rdfFilesPath)
    val validation = new ShaclValidation(path)
    validation.validate()
  }
}
