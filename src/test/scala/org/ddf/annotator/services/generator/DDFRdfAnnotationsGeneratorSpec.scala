package org.ddf.annotator.services.generator

import java.net.URI

import org.ddf.annotator.DDFDictionnaireAnnotatorTestSpec
import org.ddf.annotator.model.{CanonicalForm, Entry, LexicalEntry, Sense, VocsLexical}
import org.ddf.annotator.services.DDFDictionnaireRdfReader

import scala.collection.mutable

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class DDFRdfAnnotationsGeneratorSpec extends DDFDictionnaireAnnotatorTestSpec {

  val canonicalForm = new CanonicalForm("test", Seq())

  val vocsLexical = new VocsLexical("country", "Test United States")

  val sense = new Sense("definition to write", None, Seq(vocsLexical), None, None, None)

  val lentry = LexicalEntry(URI.create("www.lentry.com"), Some("ontolex:word"),None, Some(canonicalForm), None,
    None, Seq(None), None, Some("ontolex:masculine"), None, Some("ontolex:plurial"), None, None)

  val entry = Entry(URI.create("www.url.com"), "form",Some("about form"), "definition", lentry, None)


  implicit val rdfReader = new DDFDictionnaireRdfReader()
  rdfReader.init
  val posType: mutable.Map[String, String] = rdfReader.posTypeBlocked

  it should "generator" in {

  }
}
