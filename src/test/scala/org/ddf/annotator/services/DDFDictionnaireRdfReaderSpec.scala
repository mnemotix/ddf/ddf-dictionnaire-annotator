package org.ddf.annotator.services


import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.ExecutionContext

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class DDFDictionnaireRdfReaderSpec extends AnyFlatSpec with Matchers {

  implicit val executionContext = ExecutionContext.global

  val rdfReader =  new DDFDictionnaireRdfReader
  rdfReader.init

  it should "load POS and type" in {
    val postype =  rdfReader.posTypeBlocked
    postype.foreach(map => println(map))
    postype.size shouldEqual(27)
  }

  it should "load Vocs" in {
    val vocs = rdfReader.vocsBlocked
    vocs.foreach(map => println(
      map._1 +" -> "+ map._2
    ))
    println(vocs.size)
  }

  it should "load sense relation" in {
    val relations = rdfReader.senseRelationBlocked
    relations.foreach(rel => println(rel) )
  }
}
