package org.ddf.annotator.services.recognizer

import java.net.URI

import org.ddf.annotator.DDFDictionnaireAnnotatorTestSpec

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class LexicalEntryAnnotationsRecogsSpec extends DDFDictionnaireAnnotatorTestSpec {

  it should "produce an annotation for a lexicalEntry" in {
    val uriLexicalEntry = URI.create("www.uriLexicalEntry.test")
    val uriLexicogEntry = URI.create("www.uriLexicogEntry.test")

  }

  it should "produce annotations for the form" in {
    val uriLexicalEntry = URI.create("www.uriLexicalEntry.test")
    val label = "test"

  }

  it should "extract class for a bunch of word" in {
    val word = "test"
    val word2 = "test au mans"
    LexicalEntryAnnotationsRecogs.extractClassForm(word) shouldEqual("http://www.w3.org/ns/lemon/ontolex#Word")
    LexicalEntryAnnotationsRecogs.extractClassForm(word2) shouldEqual("http://www.w3.org/ns/lemon/ontolex#MultiWordExpression")
  }

  it should "produce annotations for other word" in {
    val uriLexicalEntry = URI.create("www.uriLexicalEntry.test")
    val words = Seq("world", "monde")
  }

  it should "produce annotations for pronounciation" in {
    val uriLexicalEntry = URI.create("www.uriLexicalEntry.test")
    val words = Seq("pro1", "pro2")
  }

  it should "produce annotations for etymology" in {
    val uriLexicalEntry = URI.create("www.uriLexicalEntry.test")
    val etymologyText = "this is the etymology of test"
  }
}
