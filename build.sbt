import sbt.url
import com.gilcloud.sbt.gitlab.GitlabPlugin


val meta = """META.INF(.)*""".r
enablePlugins(GitlabPlugin)
ThisBuild / useCoursier := false
ThisBuild / onChangedBuildSource := ReloadOnSourceChanges

lazy val ddfWiktionnaireAnnotator = (project in file(".")).
  settings(
    name := "ddf-annotator",
    organization := "org.ddf",
    scalaVersion := Version.scalaVersion,
    licenses := Seq("Apache 2.0" -> url("https://opensource.org/licenses/Apache-2.0")),
    developers := List(
      Developer(
        id = "prlherisson",
        name = "Pierre-René Lhérison",
        email = "pr.lherisson@mnemotix.com",
        url = url("http://www.mnemotix.com")
      ),
        Developer(
        id = "ndelaforge",
        name = "Nicolas Delaforge",
        email = "nicolas.delaforge@mnemotix.com",
        url = url("http://www.mnemotix.com")
      ),
    ),
    homepage := Some(url("https://gitlab.com/mnemotix/ddf/ddf-dictionnaire-annotator")),
    scmInfo := Some(
      ScmInfo(
        url("https://gitlab.com/mnemotix/ddf/ddf-dictionnaire-annotator.git"),
        "scm:git@gitlab.com:mnemotix/ddf/ddf-dictionnaire-annotator.git"
      )
    ),
    GitlabPlugin.autoImport.gitlabGroupId   :=  Some(6257667),
    GitlabPlugin.autoImport.gitlabProjectId :=  Some(16843866),  // Project ID
    GitlabPlugin.autoImport.gitlabDomain    :=  "gitlab.com",
    publishTo := Some("gitlab maven" at "https://gitlab.com/api/v4/projects/16843866/packages/maven"),
    credentials += Credentials(Path.userHome / ".sbt" / ".credentials.gitlab"),
    resolvers ++= Seq(
      Resolver.mavenLocal,
      Resolver.typesafeRepo("releases"),
      Resolver.typesafeIvyRepo("releases"),
      Resolver.sbtPluginRepo("releases"),
      "gitlab-maven" at "https://gitlab.com/api/v4/projects/21727073/packages/maven",
      "gitlab-maven-template-replacer" at "https://gitlab.com/api/v4/projects/35329974/packages/maven",
      "gitlab-maven-franceterme" at "https://gitlab.com/api/v4/projects/43326119/packages/maven"
    ),
    updateOptions := updateOptions.value.withGigahorse(false),
    libraryDependencies ++= Dependencies.core
  )